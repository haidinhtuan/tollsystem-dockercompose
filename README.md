# Evaluation of microservices frameworks

## Prerequisites

Login to DockerHub following the instruction: 

https://docs.docker.com/engine/reference/commandline/login/

## Setup


## Running the project (simulator only)

```bash
$ docker-compose up -d --build
```

The `-d` flag starts docker-compose in detached mode. Omit it to see the logs of all containers.

## Running the project with microservices

### Hemera

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.hemera.yml up -d --build
```

### Go Micro

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.gomicro.yml up -d --build
```

### Spring-Boot

Install maven, then :

```bash
$ sh tollsystem-springboot/compile.sh
$ docker-compose -f docker-compose.yml -f docker-compose.spring-boot.yml up -d --build
```
additional useful tool (replace "logs" by the nats queue you want) :
```bash
$ ./spring-boot-microservices/natsConnect.sh logs
```

### Lagom

Install [sbt](https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html), then :

```bash
$ cd tollsystem-lagom
$ sbt docker:publishLocal
$ cd ..
```
Then run the command

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.lagom.yml up -d --build
```

### Moleculer

Run the command

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.moleculer.yml up -d --build
```

### Stopping the containers

```bash
$ docker-compose stop
```

## Starting the simulation

To start a simulation, go to the [Poll Dashboard](http://localhost), configure the simulation and press start.

## Public Endpoints

- [Poll Dashboard](http://localhost) (Includes Weave Scope)
- [Toll Simulator](http://localhost:8760)

## Logs

Once the simulation was started, log files can be found in `PROJECT_ROOT/logs/`. 

These log files can also be loaded in the [performance tab](http://localhost/#/performance) in the dashboard

## Configuration
Several environment variables can be set in the `toll-simulator` to tweak the simulation:

| Variable | Type | Description | Default |
| :--- | :--- | :--- | :--- |
| `ACCURACY` | int | Accuracy of the emitted location in meters | `5` |
| `SIMULATION` | String (`short`, `large`, `null`)| Defines whether the short, the large or the small simulation is used | `null` |
| `INTERVAL` | int | The default interval size | `5` |
| `VEHICLES` | int | The default vehicle count | `10` |


## Compatibility Tests
We tested how well the different micro service frameworks work which each other. Therefore we created scenarios where we replaced some services with services from different frameworks.
To do this go to `compatibility-tests` and run one of the docker-compose files (for Spring services, `sh spring-boot-microservices/compile.sh` needs to be executed in the project root beforehand).
