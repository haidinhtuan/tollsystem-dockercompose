package com.haidinhtuan.mapmatcher.impl


import java.nio.charset.StandardCharsets
import java.time.{ZoneOffset, ZonedDateTime}
import java.util.Properties

import com.haidinhtuan.mapmatcher.api.{LatLon, LogMessage, MapMatcherService, MapMatcherToPollutionMatcherMessage, OsrmReturnMessage, OsrmService, SimulatorToMapMatcherMessage}
import io.nats.client.{Connection, Dispatcher, Nats, Options}
import play.api.libs.json.{JsError, JsPath, JsResult, JsSuccess, JsValue, Json}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration





/**
  * Implementation of the TollsystemService.
  */
class MapMatcherServiceImpl(externalService: OsrmService)(implicit ec: ExecutionContext) extends MapMatcherService {
  private val opts: Properties = new Properties
  opts.put("user", MapMatcherService.NATS_USER)
  opts.put("pass", MapMatcherService.NATS_PASS)
  opts.put("server", MapMatcherService.NATS_SERVER)
  opts.put("connectionName", MapMatcherService.NATS_NAME)



  val o: Options = new Options.Builder().server(MapMatcherService.NATS_SERVER).connectionName(MapMatcherService.NATS_NAME).maxReconnects(-1).userInfo(MapMatcherService.NATS_USER.toCharArray, MapMatcherService.NATS_PASS.toCharArray)build
  val nc: Connection = Nats.connect(o)

  val d: Dispatcher = nc.createDispatcher( natsMessage => Future {
    println("New message arrive")
    val jsonMessage: JsValue = Json.parse(natsMessage.getData)
    val messageParsed: JsResult[SimulatorToMapMatcherMessage] = Json.fromJson[SimulatorToMapMatcherMessage](jsonMessage)

    messageParsed match {
      case JsSuccess(m: SimulatorToMapMatcherMessage, path: JsPath) => processMessage(m)
      case e: JsError => println("Errors: " + JsError.toJson(e).toString())
    }
  })

  d.subscribe(MapMatcherService.MAP_MATCHER_TOPIC)

  var vehicleWithPositions : scala.collection.concurrent.Map[String, VehiclePosition] = scala.collection.concurrent.TrieMap()

  def sendMessage(subject: String, message: String): Unit = nc.synchronized {
    nc.publish(subject, message.getBytes(StandardCharsets.UTF_8))
  }

  def processMessage(message: SimulatorToMapMatcherMessage): Unit = {
    sendLog(message, "received")

    if (!vehicleWithPositions.contains(message.carId)) {
      val newPosition : LatLon = LatLon(message.lat.toDouble, message.lon.toDouble)
      val newVehiclePosition : VehiclePosition = VehiclePosition(LatLon(0.0,0.0), newPosition)
      vehicleWithPositions += message.carId -> newVehiclePosition
    } else {
      val newPosition: LatLon = LatLon(message.lat.toDouble, message.lon.toDouble)
      var previousPosition: LatLon = vehicleWithPositions(message.carId).previousLocation
      var currentPosition: LatLon = vehicleWithPositions(message.carId).currentPosition
      previousPosition = currentPosition
      currentPosition = newPosition
      vehicleWithPositions(message.carId) = VehiclePosition(previousPosition, currentPosition)

      val result = queryOSRM(previousPosition.lon, previousPosition.lat, currentPosition.lon, currentPosition.lat)
      val firstCoordinate: LatLon = LatLon(result.tracepoints(0).location(1), result.tracepoints(0).location(0))
      val secondCoordinate: LatLon = LatLon(result.tracepoints(1).location(1), result.tracepoints(1).location(0))
      var route: Route = Route(firstCoordinate, secondCoordinate)
      val outgoingMessage = MapMatcherToPollutionMatcherMessage("map-matcher", "location.matched", message.messageId, ZonedDateTime.now(ZoneOffset.UTC).toString, message.carId, List(firstCoordinate,secondCoordinate), "lagom")
      val outgoingJson: JsValue = Json.toJson(outgoingMessage)
      sendMessage(MapMatcherService.POLLUTION_MATCHER_TOPIC, outgoingJson.toString())
      sendLog(message, "sent")
    }
  }

  private def queryOSRM(lon1: Double, lat1: Double, lon2: Double, lat2: Double): OsrmReturnMessage = {
    val result = Await.result(externalService.osrmMessageProcessor(lon1,lat1,lon2, lat2, "polyline").invoke(), Duration(5000, "millis"))
    result
  }

  private def sendLog(message: SimulatorToMapMatcherMessage, logType: String): Unit = {
    val outgoingMessage = LogMessage(message.messageId, "map-matcher", "lagom", ZonedDateTime.now(ZoneOffset.UTC).toString, logType)
    val outgoingJson: JsValue = Json.toJson(outgoingMessage)
    sendMessage(MapMatcherService.LOG_TOPIC, outgoingJson.toString())
  }

}

case class Route(
  firstCoordinates: LatLon,
  secondCoordinates: LatLon
)

case class VehiclePosition(
  previousLocation: LatLon,
  currentPosition: LatLon
)
