package com.haidinhtuan.mapmatcher.impl

import com.haidinhtuan.mapmatcher.api.{MapMatcherService, MapMatcherToPollutionMatcherMessage, OsrmService, SimulatorToMapMatcherData, SimulatorToMapMatcherMessage}
import com.lightbend.lagom.scaladsl.client.ConfigurationServiceLocatorComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader, LagomServer}
import com.softwaremill.macwire.wire
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable.Seq

class MapMatcherLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new MapMatcherApplication(context) with ConfigurationServiceLocatorComponents


  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new MapMatcherApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[MapMatcherService])
}

abstract class MapMatcherApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents {


  // Bind the service that this server provides
  override lazy val lagomServer : LagomServer = serverFor[MapMatcherService](wire[MapMatcherServiceImpl])

  // Register the JSON serializer registry
  lazy val jsonSerializerRegistry : JsonSerializerRegistry = MapMatcherSerializerRegistry

  lazy val osrmService : OsrmService = serviceClient.implement[OsrmService]
}

object MapMatcherSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[MapMatcherToPollutionMatcherMessage],
    JsonSerializer[SimulatorToMapMatcherMessage],
    JsonSerializer[SimulatorToMapMatcherData]
  )
}
