// The Lagom plugin
addSbtPlugin("com.lightbend.lagom" % "lagom-sbt-plugin" % "1.6.0")
// Needed for importing the project into Eclipse
//addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")
//addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.5")


addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.5.1")
//addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")
//addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.5.0")

addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.11")
