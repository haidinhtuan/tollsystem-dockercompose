package com.haidinhtuan.pollutionmatcher.api

import com.lightbend.lagom.scaladsl.api.Service
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json}



object PollutionMatcherService  {
  val POLLUTION_MATCHER_SERVICE = "location.matched"
  val TOLL_CALCULATOR_TOPIC = "pollution.matched"
  val LOG_TOPIC = "logs"
  val NATS_USER = "ruser"
  val NATS_PASS = "T0pS3cr3t"
//  val NATS_SERVER = "127.0.0.1:4222"
  val NATS_SERVER = "nats-server:4222"
  val NATS_NAME = "PollutionMatcherService"
}

/**
  * The tollsystem service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the TollsystemService.
  */
trait PollutionMatcherService extends Service {

  override final def descriptor = {
    import Service._
    named("PollutionMatcher")                 // Name used for the ServiceLocator
      .withAutoAcl(true)
  }
}

case class MapMatcherToPollutionMatcherMessage(sender: String, topic: String, messageId: Int, timestamp: String, carId: String, route: List[LatLon], framework: String)

object MapMatcherToPollutionMatcherMessage {
  implicit val format: Format[MapMatcherToPollutionMatcherMessage] = (
    (JsPath \ "data" \ "sender").format[String] and
      (JsPath \ "data" \ "topic").format[String] and
      (JsPath \ "data" \  "messageId").format[Int] and
      (JsPath \ "data" \ "timestamp").format[String] and
      (JsPath \ "data" \ "carId").format[String] and
      (JsPath \ "data" \ "route").format[List[LatLon]] and
      (JsPath \ "data" \ "framework").format[String]
    )(MapMatcherToPollutionMatcherMessage.apply, unlift(MapMatcherToPollutionMatcherMessage.unapply))
}

case class PollutionMatcherToTollCalculatorMessage(messageId: Int, timestamp: String, carId: String, sender: String, topic: String, segment: Seq[Segment], framework: String)

object PollutionMatcherToTollCalculatorMessage {
  implicit val format: Format[PollutionMatcherToTollCalculatorMessage] = (
    (JsPath \ "data" \ "messageId").format[Int] and
      (JsPath \ "data" \ "timestamp").format[String] and
      (JsPath \ "data" \ "carId").format[String] and
      (JsPath \ "data" \ "sender").format[String] and
      (JsPath \ "data" \ "topic").format[String] and
      (JsPath \ "data" \ "segments").format[Seq[Segment]] and
      (JsPath \ "data" \ "framework").format[String]
    )(PollutionMatcherToTollCalculatorMessage.apply, unlift(PollutionMatcherToTollCalculatorMessage.unapply))
}

case class LogMessage(messageId: Int, sender: String, framework: String, timestamp: String, logType: String)

object LogMessage {
  implicit val format: Format[LogMessage] = (
    (JsPath \ "data" \  "messageId").format[Int] and
      (JsPath \ "data" \ "sender").format[String] and
      (JsPath \ "data" \ "framework").format[String] and
      (JsPath \ "data" \ "timestamp").format[String] and
      (JsPath \ "data" \ "type").format[String]
    )(LogMessage.apply, unlift(LogMessage.unapply))
}

case class LatLon(
                   lat: Double,
                   lon: Double
                 )

object LatLon {
  implicit val latLonFormat : Format[LatLon] = Json.format[LatLon]
}

case class Segment(
  segmentId: Int,
  pollutionLevel: Int,
  segmentSections: Seq[LatLon]
)

object Segment {
  implicit val segmentFormat : Format[Segment] = Json.format[Segment]
}
