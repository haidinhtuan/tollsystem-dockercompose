package com.haidinhtuan.mapmatcher.api

import com.lightbend.lagom.scaladsl.api.{Descriptor, Service}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json}


object MapMatcherService  {
  val MAP_MATCHER_TOPIC = "location.update"
  val POLLUTION_MATCHER_TOPIC = "location.matched"
  val LOG_TOPIC = "logs"
  val NATS_USER = "user"
  val NATS_PASS = "pass"
//  val NATS_SERVER = "127.0.0.1:4222"
  val NATS_SERVER = "nats-server:4222"
  val NATS_NAME = "MapMatcherService"
}

/**
  * The tollsystem service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the TollsystemService.
  */
trait MapMatcherService extends Service {

  override final def descriptor : Descriptor = {
    import Service._
    named("mapmatcher") // Name used for the ServiceLocator
      .withAutoAcl(true)
  }
}


case class SimulatorToMapMatcherData(messageId: Int, timestamp: String, accuracy: Int, carId: String, lat: Float, lon: Float)

object SimulatorToMapMatcherData {
  implicit val format: Format[SimulatorToMapMatcherData] = (
    (JsPath \ "messageId").format[Int] and
      (JsPath \ "timestamp").format[String] and
      (JsPath \ "accuracy").format[Int] and
      (JsPath \ "carId").format[String] and
      (JsPath \ "lat").format[Float] and
      (JsPath \ "lon").format[Float]
    )(SimulatorToMapMatcherData.apply, unlift(SimulatorToMapMatcherData.unapply))
}

//case class SimulatorToMapMatcherMessage(data: SimulatorToMapMatcherData)
//
//object SimulatorToMapMatcherMessage {
//  implicit val format: Format[SimulatorToMapMatcherMessage] =
//      (JsPath \ "data").format[SimulatorToMapMatcherData]
//  (SimulatorToMapMatcherData.apply, unlift(SimulatorToMapMatcherData.unapply))
//}


case class SimulatorToMapMatcherMessage(messageId: Int, sender: String, topic: String, timestamp: String, accuracy: Int, carId: String, lat: Float, lon: Float)

object SimulatorToMapMatcherMessage {
  implicit val format: Format[SimulatorToMapMatcherMessage] = (
    (JsPath \ "data" \ "messageId").format[Int] and
      (JsPath \ "data" \ "sender").format[String] and
      (JsPath \ "data" \ "topic").format[String] and
      (JsPath \ "data" \ "timestamp").format[String] and
      (JsPath \ "data" \ "accuracy").format[Int] and
      (JsPath \ "data" \ "carId").format[String] and
      (JsPath \ "data" \ "lat").format[Float] and
      (JsPath \ "data" \ "lon").format[Float]
    )(SimulatorToMapMatcherMessage.apply, unlift(SimulatorToMapMatcherMessage.unapply))
}

case class MapMatcherToPollutionMatcherMessage(sender: String, topic: String, messageId: Int, timestamp: String, carId: String, route: List[LatLon], framework: String)

object MapMatcherToPollutionMatcherMessage {
  implicit val format: Format[MapMatcherToPollutionMatcherMessage] = (
    (JsPath \ "data" \ "sender").format[String] and
      (JsPath \ "data" \ "topic").format[String] and
      (JsPath \ "data" \  "messageId").format[Int] and
      (JsPath \ "data" \ "timestamp").format[String] and
      (JsPath \ "data" \ "carId").format[String] and
      (JsPath \ "data" \ "route").format[List[LatLon]] and
      (JsPath \ "data" \ "framework").format[String]
    )(MapMatcherToPollutionMatcherMessage.apply, unlift(MapMatcherToPollutionMatcherMessage.unapply))
}

case class LogMessage(messageId: Int, sender: String, framework: String, timestamp: String, logType: String)

object LogMessage {
  implicit val format: Format[LogMessage] = (
    (JsPath \ "data" \  "messageId").format[Int] and
      (JsPath \ "data" \ "sender").format[String] and
      (JsPath \ "data" \ "framework").format[String] and
      (JsPath \ "data" \ "timestamp").format[String] and
      (JsPath \ "data" \ "type").format[String]
    )(LogMessage.apply, unlift(LogMessage.unapply))
}

case class LatLon(
  lat: Double,
  lon: Double
)

object LatLon {
  implicit val latLonFormat : Format[LatLon] = Json.format[LatLon]
}
