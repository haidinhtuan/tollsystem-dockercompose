package com.haidinhtuan.mapmatcher.api

import com.lightbend.lagom.scaladsl.api.transport.Method
import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}

object OsrmService {

}

/**
  * The external OSRM service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the OsrmService.
  */
trait OsrmService extends Service {

  def osrmMessageProcessor(lon1: Double, lat1: Double, lon2: Double, lat2: Double, geometries: String): ServiceCall[NotUsed, OsrmReturnMessage]

  override final def descriptor: Descriptor = {
    import Service._
    // @formatter:off
    named("osrm-service") // Name used for the ServiceLocator
      .withCalls(
      restCall(Method.GET, "/match/v1/car/:lon1,:lat1;:lon2,:lat2?geometries", osrmMessageProcessor _)
    )
      .withAutoAcl(true)
    // @formatter:on
  }
}

case class OsrmReturnMessage(
                            code: String,
                            tracepoints: List[TracePoint],
                            matchings: List[Matching],
                            )

object OsrmReturnMessage {
  implicit val format: Format[OsrmReturnMessage] = Json.format[OsrmReturnMessage]
}

case class TracePoint(
                       alternatives_count: Int,
                       location: Seq[Double],
                       hint: String,
                       name: String,
                       matchings_index: Int,
                       waypoint_index: Int
                     )

object TracePoint {
  implicit val format: Format[TracePoint] = Json.format[TracePoint]
}

case class Matching (
                    duration: Double,
                    distance: Double,
                    weight: Double,
                    geometry: String,
                    confidence: Double,
                    weight_name: String,
                    legs: List[Leg]
                    )

object Matching {
  implicit val format: Format[Matching] = Json.format[Matching]

}

case class Leg (
               weight: Double,
               distance: Double,
               summary: String,
               duration: Double
               )
object Leg {
  implicit val format: Format[Leg] = Json.format[Leg]

}
