package com.haidinhtuan.pollutionmatcher.impl

import java.nio.charset.StandardCharsets
import java.time.{ZoneOffset, ZonedDateTime}
import java.util.Properties

import com.haidinhtuan.pollutionmatcher.api.{LatLon, LogMessage, MapMatcherToPollutionMatcherMessage, PollutionMatcherService, PollutionMatcherToTollCalculatorMessage, Segment}
import com.lightbend.lagom.scaladsl.persistence.jdbc.JdbcSession
import play.api.libs.json.{JsError, JsPath, JsResult, JsSuccess, JsValue, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import io.nats.client.{Connection, Dispatcher, Nats, Options}
import JdbcSession.tryWith


/**
  * Implementation of the TollsystemService.
  */
class PollutionMatcherServiceImpl(session: JdbcSession)(implicit ec: ExecutionContext) extends PollutionMatcherService {


  private val opts: Properties = new Properties
  opts.put("user", PollutionMatcherService.NATS_USER)
  opts.put("pass", PollutionMatcherService.NATS_PASS)
  opts.put("server", PollutionMatcherService.NATS_SERVER)

  val o: Options = new Options.Builder().server(PollutionMatcherService.NATS_SERVER).connectionName(PollutionMatcherService.NATS_NAME).maxReconnects(-1).userInfo(PollutionMatcherService.NATS_USER.toCharArray, PollutionMatcherService.NATS_PASS.toCharArray)build
  val nc: Connection = Nats.connect(o)

  val d: Dispatcher = nc.createDispatcher( natsMessage => Future {
    println("New message arrive")
    val jsonMessage: JsValue = Json.parse(natsMessage.getData)
    val messageParsed: JsResult[MapMatcherToPollutionMatcherMessage] = Json.fromJson[MapMatcherToPollutionMatcherMessage](jsonMessage)

    messageParsed match {
      case JsSuccess(m: MapMatcherToPollutionMatcherMessage, path: JsPath) => processMessage(m)
      case e: JsError => println("Errors: " + JsError.toJson(e).toString())
    }
  })

  d.subscribe(PollutionMatcherService.POLLUTION_MATCHER_SERVICE)

  private def sendMessage(subject: String, message: String): Unit = nc.synchronized {
    nc.publish(subject, message.getBytes(StandardCharsets.UTF_8))
  }

  private def processMessage(message: MapMatcherToPollutionMatcherMessage): Unit = {
    sendLog(message, "received")

    val linestring = lineStringBuilder(message)

    val segments = Await.result(getMessage(linestring), Duration(5000, "millis"))

    val outgoingMessage = PollutionMatcherToTollCalculatorMessage(message.messageId, ZonedDateTime.now(ZoneOffset.UTC).toString, message.carId, "pollution-matcher", "pollution.matched", segments, "lagom")
    val outgoingJson: JsValue = Json.toJson(outgoingMessage)
    sendMessage(PollutionMatcherService.TOLL_CALCULATOR_TOPIC, outgoingJson.toString())
    sendLog(message, "sent")
  }

  private def lineStringBuilder(message: MapMatcherToPollutionMatcherMessage) : String = {
    var result: String = "'LINESTRING("
    result += message.route(0).lon + " " + message.route(0).lat + "," + message.route(1).lon + " " + message.route(1).lat + ")'"
    return result
  }

  private def sendLog(message: MapMatcherToPollutionMatcherMessage, logType: String): Unit = {
    val outgoingMessage = LogMessage(message.messageId, "pollution-matcher", "lagom", ZonedDateTime.now(ZoneOffset.UTC).toString, logType)
    val outgoingJson: JsValue = Json.toJson(outgoingMessage)
    sendMessage(PollutionMatcherService.LOG_TOPIC, outgoingJson.toString())
  }

  private def getMessage(linestring: String): Future[Seq[Segment]] = {
    val query = "SELECT ST_AsGeoJson(ST_intersection (a.outline, ST_GeomFromText(" + linestring +
      ", 4326))) as geometry, a.pollution FROM berlin_polygons a WHERE not ST_IsEmpty(ST_AsText(ST_intersection (a.outline, ST_GeomFromText(" + linestring +
      ",4326))));"

    val sql = query

    var returnSegments = Seq[Segment]()

    session.withConnection(con => {
      tryWith(con.prepareStatement(sql)) { statement => {
        val res = statement.executeQuery()
        res.toString
        var segmentId = 1
        var segments = Seq[Segment]()

        while (res.next()) {
          var jsonString = res.getString("geometry")
          var pollutionLevel : Int = res.getInt("pollution")
          val json: JsValue = Json.parse(jsonString)
          Json.prettyPrint(json)

          var lat1 = (json \ "coordinates" \ 0).get(1)
          var lon1 = (json \ "coordinates" \ 0).get(0)
          var lat2 = (json \ "coordinates" \ 1).get(1)
          var lon2 = (json \ "coordinates" \ 1).get(0)
          var firstCoordinates : LatLon = LatLon(lon1.as[Double], lat1.as[Double])
          var secondCoordinates : LatLon = LatLon(lon2.as[Double], lat2.as[Double])
          var segmentSection : Segment = Segment(segmentId, pollutionLevel, Seq(firstCoordinates,secondCoordinates))

          segmentId += 1
          segments = segments:+segmentSection
        }
        returnSegments = segments
      }}
      returnSegments
    })
  }




}
