package com.haidinhtuan.pollutionmatcher.impl

import com.haidinhtuan.pollutionmatcher.api.{MapMatcherToPollutionMatcherMessage, PollutionMatcherService}
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.jdbc.JdbcPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader, LagomServer}
import com.softwaremill.macwire.wire
import play.api.db.{ConnectionPool, HikariCPConnectionPool}
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable.Seq

class PollutionMatcherLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new PollutionMatcherApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new PollutionMatcherApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[PollutionMatcherService])
}

abstract class PollutionMatcherApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with JdbcPersistenceComponents
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer : LagomServer = serverFor[PollutionMatcherService](wire[PollutionMatcherServiceImpl])

  override def connectionPool: ConnectionPool = new HikariCPConnectionPool(environment)

  //wire[NatsUtils]

  // Register the JSON serializer registry
  override lazy val jsonSerializerRegistry : JsonSerializerRegistry = PollutionMatcherSerializerRegistry

  // Register the tollsystem persistent entity
  //persistentEntityRegistry.register(wire[PollutionMatcherEntity])
  // Register the Repository
  //val userNatsClient = wire[NatsUtils]
}

object PollutionMatcherSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[MapMatcherToPollutionMatcherMessage]
  )
}
