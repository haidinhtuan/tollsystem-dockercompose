package com.haidinhtuan.tollcalculator.impl

import com.haidinhtuan.tollcalculator.api.{PollutionMatcherToTollCalculatorMessage, TollCalculatorService}
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader, LagomServer}
import com.softwaremill.macwire.wire
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable.Seq

class TollCalculatorLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TollCalculatorApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TollCalculatorApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[TollCalculatorService])
}

abstract class TollCalculatorApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    //with CassandraPersistenceComponents
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer : LagomServer = serverFor[TollCalculatorService](wire[TollCalculatorServiceImpl])

  //wire[NatsUtils]

  // Register the JSON serializer registry
  lazy val jsonSerializerRegistry : JsonSerializerRegistry = TollCalculatorSerializerRegistry

  // Register the tollsystem persistent entity
  //persistentEntityRegistry.register(wire[TollCalculatorEntity])

}

object TollCalculatorSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[PollutionMatcherToTollCalculatorMessage]
  )
}