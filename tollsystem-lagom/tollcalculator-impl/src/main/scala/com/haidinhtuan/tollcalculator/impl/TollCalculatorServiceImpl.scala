package com.haidinhtuan.tollcalculator.impl

import java.nio.charset.StandardCharsets
import java.time.{ZoneOffset, ZonedDateTime}
import java.util.Properties

import com.haidinhtuan.tollcalculator.api.{LogMessage, PollutionMatcherToTollCalculatorMessage, TollCalculatorService, TollCalculatorToDashboardMessage}
import play.api.libs.json.{JsError, JsPath, JsResult, JsSuccess, JsValue, Json}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import io.nats.client.{Connection, Dispatcher, Nats, Options}


/**
  * Implementation of the TollsystemService.
  */
class TollCalculatorServiceImpl(implicit ec: ExecutionContext) extends TollCalculatorService {

  private val opts: Properties = new Properties
  opts.put("user", TollCalculatorService.NATS_USER)
  opts.put("pass", TollCalculatorService.NATS_PASS)
  opts.put("server", TollCalculatorService.NATS_SERVER)

  var vehicleWithFares : scala.collection.concurrent.Map[String, Double] = scala.collection.concurrent.TrieMap()


  val o: Options = new Options.Builder().server(TollCalculatorService.NATS_SERVER).connectionName(TollCalculatorService.NATS_NAME).maxReconnects(-1).userInfo(TollCalculatorService.NATS_USER.toCharArray, TollCalculatorService.NATS_PASS.toCharArray)build
  val nc: Connection = Nats.connect(o)

  val d: Dispatcher = nc.createDispatcher( natsMessage => Future {
    println("New message arrive")
    val jsonMessage: JsValue = Json.parse(natsMessage.getData)
    val messageParsed: JsResult[PollutionMatcherToTollCalculatorMessage] = Json.fromJson[PollutionMatcherToTollCalculatorMessage](jsonMessage)

    messageParsed match {
      case JsSuccess(m: PollutionMatcherToTollCalculatorMessage, path: JsPath) => processMessage(m)
      case e: JsError => println("Errors: " + JsError.toJson(e).toString())
    }
  })

  d.subscribe(TollCalculatorService.TOLL_CALCULATOR_TOPIC)

  private def sendMessage(subject: String, message: String): Unit = nc.synchronized {
    nc.publish(subject, message.getBytes(StandardCharsets.UTF_8))
  }

  def calculateDistance(startLon: Double, startLat: Double, endLon: Double, endLat: Double, R: Double): Double = {
    val dLat = math.toRadians(endLat - startLat)
    val dLon = math.toRadians(endLon - startLon)
    val lat1 = math.toRadians(startLat)
    val lat2 = math.toRadians(endLat)

    val a =
      math.sin(dLat / 2) * math.sin(dLat / 2) +
        math.sin(dLon / 2) * math.sin(dLon / 2) * math.cos(lat1) * math.cos(lat2)
    val c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    R * c / 1000
  }

  private def calculateFare(message: PollutionMatcherToTollCalculatorMessage): Double = {

    var fare : Double = 0d
    for ( x <- 1 to message.segment.length) {
      var distance = calculateDistance(message.segment.seq(x-1).segmentSections(0).lon,
        message.segment.seq(x-1).segmentSections(0).lat,
        message.segment.seq(x-1).segmentSections(1).lon,
        message.segment.seq(x-1).segmentSections(1).lat, 6378137d)
      fare += distance * message.segment.seq(x-1).pollutionLevel / 10
    }
    fare
  }

  private def processMessage(message: PollutionMatcherToTollCalculatorMessage): Unit = {
    println("[Toll Calculator] Received message for vehicle: " + message.carId)
    sendLog(message, "received")

    if (!vehicleWithFares.contains(message.carId)) {
      var updatedFare = calculateFare(message)
      vehicleWithFares += message.carId -> updatedFare
    } else {
      var updatedFare = calculateFare(message)
      vehicleWithFares(message.carId) = vehicleWithFares(message.carId) + updatedFare
    }

    val outgoingMessage = TollCalculatorToDashboardMessage("toll-calculator", "toll-calculated", message.messageId, message.carId, ZonedDateTime.now(ZoneOffset.UTC).toString, vehicleWithFares(message.carId), "lagom")
    val outgoingJson: JsValue = Json.toJson(outgoingMessage)
    sendMessage(TollCalculatorService.DASHBOARD_TOPIC, outgoingJson.toString())
    sendLog(message, "sent")
  }

  private def sendLog(message: PollutionMatcherToTollCalculatorMessage, logType: String): Unit = {
    val outgoingMessage = LogMessage(message.messageId, "toll-calculator", "lagom", ZonedDateTime.now(ZoneOffset.UTC).toString, logType)
    val outgoingJson: JsValue = Json.toJson(outgoingMessage)
    sendMessage(TollCalculatorService.LOG_TOPIC, outgoingJson.toString())
  }
}

