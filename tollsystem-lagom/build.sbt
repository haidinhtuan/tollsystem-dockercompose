import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}

organization in ThisBuild := "com.haidinhtuan"
version in ThisBuild := "1.0-SNAPSHOT"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.12.8"
lagomCassandraEnabled in ThisBuild := false
lagomKafkaEnabled in ThisBuild := false
//lagomUnmanagedServices in ThisBuild := Map("osrm-service" -> "http://127.0.0.1:5000") // Register OSRM service with the ServiceLocator
lagomUnmanagedServices in ThisBuild := Map("osrm-service" -> "http://osrm:5000") // Register OSRM service with the ServiceLocator


val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.3" % "provided"
val jdbc = "org.postgresql" % "postgresql" % "42.2.8"
val javaNatsClient = "io.nats" % "jnats" % "2.6.6"


lazy val `tollsystem` = (project in file("."))
  .aggregate(`mapmatcher-api`, `mapmatcher-impl`, `pollutionmatcher-api`, `pollutionmatcher-impl`, `tollcalculator-api`, `tollcalculator-impl`)


lazy val `mapmatcher-api` = (project in file("mapmatcher-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `mapmatcher-impl` = (project in file("mapmatcher-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      macwire,
      javaNatsClient
    ),
    lagomServiceHttpPort := 9001,
    dockerAlias := DockerAlias(Option(null), Option(null), "tollsystem-dockercompose_lagom-map-matcher", Option(null)),
    dockerBaseImage := "openjdk:8-jre-alpine",
    dockerCommands ++= Seq(
      Cmd("USER", "root"),
      ExecCmd("RUN", "apk" ,"add", "--no-cache", "bash")
    )
  )
  .dependsOn(`mapmatcher-api`,`osrm-api`)

lazy val `pollutionmatcher-api` = (project in file("pollutionmatcher-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
    )
  )

lazy val `pollutionmatcher-impl` = (project in file("pollutionmatcher-impl"))
  .enablePlugins(LagomScala, JavaAppPackaging)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceJdbc,
      macwire,
      jdbc,
      javaNatsClient
    ),
    lagomServiceHttpPort := 9002,
    dockerAlias := DockerAlias(Option(null), Option(null), "tollsystem-dockercompose_lagom-pollution-matcher", Option(null)),
    dockerBaseImage := "openjdk:8-jre-alpine",
    dockerCommands ++= Seq(
      Cmd("USER", "root"),
      ExecCmd("RUN", "apk" ,"add", "--no-cache", "bash")
    )
  )
  .dependsOn(`pollutionmatcher-api`)

lazy val `tollcalculator-api` = (project in file("tollcalculator-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
    )
  )

lazy val `tollcalculator-impl` = (project in file("tollcalculator-impl"))
  .enablePlugins(LagomScala, JavaAppPackaging)
  .settings(
    libraryDependencies ++= Seq(
      macwire,
      javaNatsClient
    ),
    lagomServiceHttpPort := 9003,
    dockerAlias := DockerAlias(Option(null), Option(null), "tollsystem-dockercompose_lagom-toll-calculator", Option(null)),
    dockerBaseImage := "openjdk:8-jre-alpine",
    dockerCommands ++= Seq(
      Cmd("USER", "root"),
      ExecCmd("RUN", "apk" ,"add", "--no-cache", "bash")
    )
  )
  .dependsOn(`tollcalculator-api`)

lazy val `osrm-api` = (project in file("osrm-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
    )
  )

