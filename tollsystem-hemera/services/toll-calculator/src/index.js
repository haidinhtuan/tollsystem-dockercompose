const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
const nats = require('nats').connect(process.env.NATS_URI);
const haversine = require('haversine')
var natsLogger = require('./controllers/logger');


const hemera = new Hemera(nats, {
    logLevel: 'info'
});
var controller = require('./controllers/controller')();

// set payload validator of your choice
hemera.use(HemeraJoi);

const start = async () => {
    try {
        // establish connection and bootstrap hemera
        await hemera.ready();

        nats.subscribe('pollution.matched', event => {
            console.log('recived: ' + event);
            let parsedEvent = JSON.parse(event);
            if(parsedEvent.data.segments.length > 0){

                controller.calculatePrice(parsedEvent, function (err, result) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(result);
                        const messageId = result.messageId;
                        const carId = result.carId;
                        controller.sendLog('received', messageId);

                        const payload = {
                            data: {
                                sender: "toll-calculator",
                                topic: "toll.calculated",
                                messageId: messageId,
                                carId: carId,
                                timestamp: new Date().toISOString(),
                                toll: result.toll
                            }
                        };

                        controller.sendTollCalculatedEvent(payload);
                    }
                })
            }
            // controller.calculateDistance(event);

        });

        hemera.log.info('service listening')
    } catch (err) {
        hemera.log.error(err);
        process.exit(1)
    }
};
start();