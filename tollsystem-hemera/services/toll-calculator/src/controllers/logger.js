const nats = require('nats').connect(process.env.NATS_URI);

module.exports = class Logger {
    publishMessage(payload) {
        nats.publish('logs', JSON.stringify(payload), function (err, result) {
            if (err) {
                console.log('error');
            }
            else {
                console.log('msg processed!');
                //then handle the call back.
            }
        });
    }

    logReceived(messageId) {
        let data = {};
        data.sender = 'toll-calculator';
        data.framework = 'hemera';
        data.type = 'received';
        data.messageId = messageId;
        data.timestamp = new Date().toISOString();
        let payload = {};
        payload.data = data;

        this.publishMessage(payload);

    }

    logSent(messageId) {
        let data = {};
        data.sender = 'toll-calculator';
        data.framework = 'hemera';
        data.type = 'sent';
        data.messageId = messageId;
        data.timestamp = new Date().toISOString();
        let payload = {};
        payload.data = data;

        this.publishMessage(payload);
    }
};