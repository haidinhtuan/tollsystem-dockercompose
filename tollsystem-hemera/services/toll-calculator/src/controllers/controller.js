var controller = () => {

    const nats = require('nats').connect(process.env.NATS_URI);
    var natsLogger = require('./logger');
    const haversine = require('haversine');
    var priceList = {
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 7,
        8: 8,
        9: 9,
        10: 10,
        11: 11,
        12: 12
    };
    const pricePerCar = {};
    // Simple Publisher
    var response = `{
        “message from pollution matcher”:123}`;

    function mapMatcherapi(price, shipping) {
        //Return the sum
        return response;
    }
    function calculatePrice(payload,callback) {

        var priceListSum = 0;
        payload.data.segments.forEach(element => {
            var removeChar = element.segmentSections.replace(/[\[\]']/g, '');
            var rawValues = removeChar.split(',');
            const start = {
                latitude: rawValues[1],
                longitude: rawValues[0]
            };

            const end = {
                latitude: rawValues[3],
                longitude: rawValues[2]
            };
            var haversineResult = haversine(start, end);

            var level = priceList[element.pollutionLevel];

            priceListSum += Math.ceil(haversineResult * level);

        });
        pricePerCar[payload.data.carId] = pricePerCar[payload.data.carId] || 0;
        pricePerCar[payload.data.carId]+= priceListSum;

        var tollCalculatorMessage = {
            messageId: payload.data.messageId,
            carId: payload.data.carId,
            timeStamp: new Date().getTime(),
            toll: pricePerCar[payload.data.carId]
        };

        callback(null,tollCalculatorMessage);

    }
    function sendTollCalculatedEvent(payload) {
        console.log("toll.calculated: " + JSON.stringify(payload));
        nats.publish('toll.calculated', JSON.stringify(JSON.stringify(payload)));
        console.log(JSON.stringify(payload));
        sendLog('sent', payload.data.messageId);
    }

    function sendLog(type, messageId) {
        const payload = {
            sender: 'toll-calculator',
            framework: 'hemera',
            type: type,
            messageId: messageId,
            timestamp: new Date().toISOString(),
        };
        nats.publish('logs', JSON.stringify({ data: payload }));
    }

    return {
        mapMatcherapi: mapMatcherapi,
        sendTollCalculatedEvent: sendTollCalculatedEvent,
        sendLog: sendLog,
        calculatePrice: calculatePrice
    }
};
module.exports = controller;
