const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
//const nats = require('nats').connect(process.env.NATS_TRANSPORT + process.env.NATS_USER +':'+ process.env.NATS_PASSWORD +'@'+ process.env.NATS_URL);
const nats = require('nats').connect(process.env.NATS_URI);


const hemera = new Hemera(nats, {
    logLevel: 'info'
});
var controller = require('./controllers/controller')();
var natsLogger = require('./controllers/logger.js');

// set payload validator of your choice
hemera.use(HemeraJoi);

const start = async () => {
    try {
        // establish connection and bootstrap hemera
        await hemera.ready();


        nats.subscribe('location.matched', function (msg) {
            let parsedMsg = JSON.parse(msg);
            console.log('received: ' +msg);

            var payload =
                {
                    data: {
                        sender: "pollution-matcher",
                        topic: "pollution.matched",
                        messageId : parsedMsg.data.messageId,
                        carId: parsedMsg.data.carId,
                        timestamp: new Date().toISOString(),
                        segments : []
                    }
                };
            //console.log('received: ' +msg);
            natsLogger.prototype.logReceived(parsedMsg.data.messageId);

            controller.pollutionMatcherapi(payload, parsedMsg.data.route);

            //natsLogger.prototype.logSent(parsedMsg.data.messageId);

        });

        hemera.log.info('service listening')
    } catch (err) {
        hemera.log.error(err);
        process.exit(1)
    }
};
start();