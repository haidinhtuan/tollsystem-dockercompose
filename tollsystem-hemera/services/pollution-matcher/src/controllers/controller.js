var controller = () => {
    const Pool = require('pg').Pool;
    const pool = new Pool({
        user: 'docker',
        host: 'postgis',
        database: 'gis',
        password: 'docker',
        port: 5432,
    });
    var natsLogger = require('./logger.js');
    const nats = require('nats').connect(process.env.NATS_URI);

    function pollutionMatcherapi(payload, route) {
        let geoJson = 'ST_GeomFromText(\'LINESTRING(';
        console.log('\n Length of route: ' + route.length);
        for (var i=0; i<route.length; i++) {
            console.log(route[i].lat);
             geoJson += route[i].lon;
             geoJson += ' ';
             geoJson += route[i].lat;
             if(i+1 < route.length){
                 geoJson += ', ';
             }

        }
        geoJson += ')\',4326)';

        let selectQuery = 'SELECT ST_AsGeoJson(ST_intersection (a.outline, ' + geoJson + ')) as geometry, a.pollution, a.id FROM berlin_polygons a WHERE not ST_IsEmpty(ST_AsText(ST_intersection (a.outline, ' + geoJson + ')))';


        pool.query(selectQuery, (error, results) => {
            if (error) {
                console.log(error);
                //throw error
            }else{

                var segments = [];

                if(results.rowCount ===0){
                    console.log ( "ZEEERO: " + selectQuery);
                    console.log("\nresult zero: " + JSON.stringify(results));
                }else{
                    for(let idx in results.rows){
                        let row = JSON.parse(results.rows[idx].geometry);
                        let s = {
                            'segmentId': results.rows[idx].id,
                            'pollutionLevel': results.rows[idx].pollution,
                            'segmentSections': JSON.stringify(row.coordinates)
                        };
                        segments.push(s);
                    }
                    console.log('segments: ' + JSON.stringify(segments));
                    payload.data.segments = segments;
                }
                publishPollutionLevel(payload);

            }

        });
        //natsLogger.prototype.logSent(payload.messageId);

        //return payload;
    }

    function publishPollutionLevel(payload) {
        const topic = payload.data.topic;
        nats.publish(topic, JSON.stringify(payload), function () {
            console.log('\n publish to toll-calculator ' + JSON.stringify(payload));
            natsLogger.prototype.logSent(payload.data.messageId);
        });
        return payload;
    }


    return {
        pollutionMatcherapi: pollutionMatcherapi,
        publishPollutionLevel: publishPollutionLevel,
    }
};
module.exports = controller;