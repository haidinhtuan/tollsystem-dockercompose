const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
//const nats = require('nats').connect(process.env.NATS_TRANSPORT + process.env.NATS_USER +':'+ process.env.NATS_PASSWORD +'@'+ process.env.NATS_URL);
const nats = require('nats').connect(process.env.NATS_URI);

const hemera = new Hemera(nats, {
    logLevel: 'info'
});

var Buffer = require("./controllers/Buffer");
var natsLogger = require('./controllers/logger.js');


// set payload validator of your choice
hemera.use(HemeraJoi);

const start = async () => {
    try {
        // establish connection and bootstrap hemera
        await hemera.ready();
        console.log("Connected to " + nats.currentServer.url.host);

        var buffer = new Buffer();
        nats.subscribe('location.update', msg => {
            console.log('received: ' + msg);
            var parsedMsg = JSON.parse(msg);
            natsLogger.prototype.logReceived(parsedMsg.data.messageId);

        buffer.pushPosition(parsedMsg.data);

        });

    } catch (err) {
        hemera.log.error(err);
        process.exit(1)
    }
};
start();