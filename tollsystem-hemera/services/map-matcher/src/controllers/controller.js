var controller = () => {

    //var request = require('request');
    var axios = require('axios');
    const nats = require('nats').connect(process.env.NATS_URI);
    var natsLogger = require('./logger.js');

    var osmMapMatcher = function (lats, longs, carId, messageId, callback) {

        var endpointaddress = process.env.OSRM_URI + '/match/v1/car/';
        console.log('\n logs: ' + longs);
        console.log('\n lats: ' + lats);

        var finalURL = endpointaddress + longs[0] + ',' + lats[0] + ';' + longs[1] + ',' + lats[1];
        console.log('\n final url: ' + finalURL);

        axios.get(finalURL)
            .then(function (response) {
                console.log("The response is:", response.data);
                const payload = {
                    data: {
                        sender: 'map-matcher',
                        topic: 'location.matched',
                        messageId: messageId,
                        carId: carId,
                        timeStamp: new Date().toISOString(),
                        route: [
                            {
                                lat: response.data.tracepoints[0].location[1],
                                lon: response.data.tracepoints[0].location[0]
                            },
                            {
                                lat: response.data.tracepoints[1].location[1],
                                lon: response.data.tracepoints[1].location[0]
                            }]
                    }
                };

                console.log("result of osrm: " + JSON.stringify(payload));
                callback(null, payload);
            })
            .catch(function (error) {
                console.log("for id: " + carId+ "an error has occured:", error);
                callback(error, null);
            });

    };
    var publishMapMatcherData = function (data, callback) {
        var tst = JSON.stringify(data);
        nats.publish('location.matched', tst, function (err, result) {
            if (err) {
                console.log('error');

            }
            else {
                console.log('msg processed!');
                natsLogger.prototype.logSent(data.data.messageId);
            }
        });
    };
    return {
        osmMapMatcher: osmMapMatcher,
        publishMapMatcherData: publishMapMatcherData
    }
};
module.exports = controller;