var controller = require('./controller')();

module.exports = class Buffer {

    constructor() {
        this.cars = [];
        this.lats = [];
        this.longs = [];
    }

    pushPosition(data) {
        if (this.cars.indexOf(data.carId) != -1) {
            let carIdx = this.cars.indexOf(data.carId);
            this.lats[carIdx].push(data.lat);
            this.longs[carIdx].push(data.lon);

        } else {
            this.cars.push(data.carId);
            let idx = this.cars.indexOf(data.carId);
            this.lats[idx] = [data.lat];
            this.longs[idx] = [data.lon];

        }

        let idx = this.cars.indexOf(data.carId);
        if (this.lats[idx].length === 2 && this.longs[idx].length === 2) {
            let lats = this.lats[idx].slice(0, 2);
            let longs = this.longs[idx].slice(0, 2);
            this.lats[idx].shift();
            this.longs[idx].shift();
            console.log('data.accuracy: ', data.accuracy)
            controller.osmMapMatcher(lats, longs, data.carId, data.messageId, function (err, result) {
                if (err) {
                    console.log('failed!')
                }
                else {
                    console.log('publishing to polution matcher', JSON.stringify(result));
                    controller.publishMapMatcherData(result, function (err, callback) {
                        if (err) {
                            console.log('an error has occurred!', err);
                        }
                        else {
                            console.log('successfully pushed to pollution matcher!');
                        }
                    })

                }
            });

        }
    };

};
