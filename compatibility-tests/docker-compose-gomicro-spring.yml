version: '3.0'

services:

  nats-server:
    container_name: nats-server
    hostname: nats-server
    image: 'nats:0.8.0'
    entrypoint: '/gnatsd -m 8222'
    expose:
      - 4222
    ports:
      - 4222:4222
      - 8222:8222
    healthcheck:
      test: ['CMD', 'curl', '-f', 'http://localhost:8222']
      interval: 5s
      timeout: 3s
      retries: 10

  osrm:
    container_name: osrm
    image: patreu22/ioslab-osrm-image:latest
    ports:
      - 5000:5000
    command: sh -c "osrm-routed --algorithm mld berlin-latest.osrm"

  populator:
    container_name: populator
    build: ../ioslab_polygonDB/
    depends_on:
      - postgis

  postgis:
    container_name: postgis
    image: kartoza/postgis
    environment:
      - POSTGRES_USER=docker
      - POSTGRES_PASS=docker
    ports:
      - 25432:5432

  toll-simulator:
    container_name: toll-simulator
    build:
      context: ../toll-simulator/
    image: toll-simulator
    ports:
      - 8760:8760
    environment:
      PORT: 8760
      NATS_URL: nats://nats-server:4222
      ACCURACY: 5
      SIMULATION: short # use for short simulation (~5 min)
    healthcheck:
      test: ['CMD', 'curl', '-f', 'http://localhost:8760/api/status']
      interval: 5s
      timeout: 3s
      retries: 10
    depends_on:
      - nats-server

  poll-dashboard:
    container_name: poll-dashboard
    build:
      context: ../poll-dashboard/
    image: poll-dashboard
    ports:
      - 80:80
    environment:
      NATS_URL: nats://nats-server:4222
      OSM_URL: https://tiles.wmflabs.org/osm-no-labels/{z}/{x}/{y}.png
    volumes:
      - ./logs:/usr/src/app/logs
    healthcheck:
      test: ['CMD', 'curl', '-f', 'http://localhost:4000/']
      interval: 5s
      timeout: 3s
      retries: 10
    depends_on:
      - nats-server
      - scope
      - toll-simulator

  scope:
    container_name: scope
    image: weaveworks/scope:1.10.1
    network_mode: 'host'
    pid: 'host'
    privileged: true
    labels:
      - 'works.weave.role=system'
    volumes:
      - '/var/run/docker.sock:/var/run/docker.sock:rw'
    command:
      - '--probe.docker=true'
      - '--weave=false'

  consul:
    container_name: gomicro-consul
    image: consul:latest
    command: consul agent -dev -log-level=warn -ui -client=0.0.0.0
    hostname: consul
    expose:
    - 8300
    - 8500
    - 8600
    
  gomicro-map-matcher:
    container_name: gomicro-map-matcher
    build: ../go-microservices/map-matcher/
    environment:
    - NATS_URI=nats://nats-server:4222
    - MICRO_REGISTRY=consul
    - MICRO_REGISTRY_ADDRESS=consul
    - OSRM_URI=osrm:5000
    depends_on:
    - nats-server
    - consul
    - osrm

  pollutionmatcher:
    container_name: iosl_spring_pollution_matcher
    environment:
      NATS_URL: nats://nats-server:4222
      CHANNEL_INPUT: location.matched
      CHANNEL_OUTPUT: pollution.matched
      CHANNEL_LOGS: logs
    build:
      context: ../spring-boot-microservices/PollutionMatcher/
    volumes:
    - ../PollutionMatcher/target:/target
  tollcalculator:
    container_name: iosl_spring_toll_calculator
    environment:
      NATS_URL: nats://nats-server:4222
      CHANNEL_INPUT: pollution.matched
      CHANNEL_OUTPUT: toll.calculated
      CHANNEL_LOGS: logs
    build:
      context: ../spring-boot-microservices/TollCalculator/
    volumes:
    - ../TollCalculator/target:/target