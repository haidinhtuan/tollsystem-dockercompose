'use strict';
const simulator = require('../simulator/simulator');
let messageId = 0;
let simulationPromise;

module.exports = {
  name: 'simulator',

  settings: {
    sender: 'toll-simulator',
    framework: 'moleculer',
    interval: parseInt(process.env.INTERVAL) || 5,
    vehicleCount: parseInt(process.env.VEHICLES) || 10,
    autoStart: process.env.AUTO_START === 'true',
  },

  actions: {
    start(context) {
      if (context.params.vehicles && isNaN(context.params.vehicles)) {
        this.logger.error('vehicles has an invalid type.');
        return 'Invalid vehicle type';
      }

      if (context.params.interval && isNaN(context.params.interval)) {
        this.logger.error('interval has an invalid type.');
        return 'Invalid interval type';
      }

      this.startSimulation(context.params.interval, context.params.vehicles);
      return 'Start successful';
    },

    stop() {
      this.stopSimulation();
      return 'Stop successful';
    },

    reset() {
      this.resetSimulation();
      return 'Reset successful';
    },

  },

  /**
   * Methods
   */
  methods: {
    sendUpdates(payload) {

      let timestamp = new Date().toISOString()

      // append message id to payload
      payload.messageId = messageId;
      payload.sender = this.settings.sender;
      payload.topic = 'location.update';


      // emit `location.update` event
      this.broker.emit('location.update', payload, [this.settings.sender]);

      // send logging information
      this.sendLogs(messageId, timestamp, 'sent');

      // increase message id
      messageId++;
    },

    sendReset() {
      const payload = {
        sender: this.settings.sender,
        topic: 'simulation.reset',
        messageId,
      };

      // emit `simulation.reset` event
      this.broker.emit('simulation.reset', payload, [this.settings.sender]);

      // increase message id
      messageId++;
    },

    sendStart(interval, vehicles) {
      const payload = {
        sender: this.settings.sender,
        topic: 'simulation.start',
        messageId: messageId,
        interval: interval,
        vehicles: vehicles,
      };

      // emit `simulation.start` event
      this.broker.emit('simulation.start', payload, [this.settings.sender]);

      // increase message id
      messageId++;
    },

    sendStepInformation(step, numberOfVehicles) {
      const payload = {
        sender: this.settings.sender,
        framework: this.settings.framework,
        type: 'step',
        step: step,
        numberOfVehicles: numberOfVehicles,
        timestamp: new Date().toISOString(),
      };

      this.broker.emit('logs', payload, [this.settings.sender]);
    },

    sendLogs(messageId, timestamp, type) {
      const payload = {
        sender: this.settings.sender,
        framework: this.settings.framework,
        type: type,
        messageId: messageId,
        timestamp: timestamp,
      };

      this.broker.emit('logs', payload, [this.settings.sender]);
    },

    resetSimulation() {
      messageId = 0;
      simulator.resetSimulation();
      this.sendReset();
    },

    stopSimulation() {
      simulator.stopSimulation();
      simulationPromise = undefined;
      this.sendReset();
    },

    startSimulation(itvl, vhcls) {
      if (simulationPromise) {
        this.stopSimulation();
      }

      const interval = isNaN(parseInt(itvl)) ? 5 : parseInt(itvl);
      const vehicles = isNaN(parseInt(vhcls)) ? null : parseInt(vhcls);

      this.sendStart(interval, vehicles);

      // set timeout before starting simulation to initialize logger and other services
      setTimeout(() => {
        simulationPromise = simulator.startSimulation(interval, vehicles, this.sendUpdates, this.sendStepInformation);

        simulationPromise.then(() => {
          simulationPromise = undefined;
          this.sendReset();
        });
      }, 1000);
    },
  },

  /**
   * Service started lifecycle event handler
   */
  started() {
    simulator.setLogger(this.logger);

    if (this.settings.autoStart) {
      this.startSimulation(this.settings.interval, this.settings.vehicleCount);
    }
  },

  /**
   * Service stopped lifecycle event handler
   */
  stopped() {
    simulator.stopSimulation();
  }
};