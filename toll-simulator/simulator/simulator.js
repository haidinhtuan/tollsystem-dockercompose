'use strict';

const fs = require('fs');
const XmlStream = require('xml-stream');
const randomizeLocation = require('./randomizeLocation');

let file;
let accuracy;
let timeout;
let intervalSize = 1;
let vehicleCount;
let simulationCallback;
let simulationStepCallback;
let logger;
let fileStream;
let xmlStream;

module.exports = {
	startSimulation: startSimulation,
	stopSimulation: stopSimulation,
	resetSimulation: resetSimulation,
	setLogger: setLogger,
};

init();

function init() {
	let simulation;
	if (process.env.SIMULATION === 'large') {
		simulation = '/simulation_large.xml';
	} else if (process.env.SIMULATION === 'short') {
		simulation = '/simulation_short.xml';
	} else {
		simulation = '/simulation.xml';
	}
	file = __dirname + simulation;

	accuracy = parseInt(process.env.ACCURACY) || 5;
}

function setLogger(molecularLogger) {
	logger = molecularLogger;
}

function startSimulation(simulationInterval, simulationVehicleCount, updateCallback, stepCallback) {
	stopStream();

	intervalSize = simulationInterval;
	vehicleCount = simulationVehicleCount;
	simulationCallback = updateCallback;
	simulationStepCallback  = stepCallback;

	logger.info(`The simulation started. Interval is ${intervalSize}. Vehicle count is ${vehicleCount || 'not limited'}. Accuracy is ${accuracy}m.`);

	return new Promise((resolve) => {
		readFileAsStream(file, handleAsyncResult(updateCallback, stepCallback), resolve);
	});
}

function stopSimulation() {
	if (!simulationCallback) {
		logger.error('Can not stop Simulation if no simulation started yet');
		return;
	}

	stopStream();
}

function resetSimulation() {
	if (!simulationCallback) {
		logger.error('Can not reset Simulation if no simulation started yet');
		return;
	}

	stopStream();
	startSimulation(intervalSize, vehicleCount, simulationCallback, simulationStepCallback);
}

function readFileAsStream(fileName, readCallback, finishCallback) {
	fileStream = fs.createReadStream(fileName);
	xmlStream = new XmlStream(fileStream);
	let counter = 0;

	xmlStream.collect('vehicle');
	xmlStream.on('endElement: timestep', item => {
		xmlStream.pause();
		timeout = setTimeout(() => {
			xmlStream.resume();
		}, 1000);


		counter++;
		if (counter % intervalSize === 0) {
			readCallback(item);
		}

	});

	xmlStream.on('end', () => {
		finishCallback();
		stopStream();
	});
}

function stopStream() {
	if (timeout) {
		clearTimeout(timeout);
		timeout = undefined;
	}

	if (xmlStream) {
		xmlStream = undefined;
	}

	if (fileStream) {
		fileStream.destroy();
		fileStream = undefined;
	}
}

function handleAsyncResult(updateCallback, stepCallback) {
	let trackedInLastInterval = [];

	return function(result) {
		let step = parseInt(result['$'].time);

		const vehicles = result.vehicle || [];

		if (!vehicles.length) {
			logger.debug(`At step ${step} no vehicles are on the road`);
		} else {
			logger.debug(`At step ${step} ${vehicles.length} vehicles are on the road`);

			const date = new Date().toISOString();
			let trackedVehicles;

			if (vehicleCount) {
				// filter out the vehicles that were already part of the last interval
				trackedVehicles = vehicles.filter(vehicle => {
					return trackedInLastInterval.indexOf(vehicle['$'].id) !== -1;
				});

				// if we do not have enough vehicles yet but the interval still has some add them to the simulation
				if (trackedVehicles.length < vehicleCount && vehicles.length >= trackedVehicles.length) {
					// filter only not yet used vehicles
					vehicles.filter(vehicle => {
						return trackedInLastInterval.indexOf(vehicle['$'].id) === -1;
					}).forEach(vehicle => {
						// add until vehicle count is reached
						if (trackedVehicles.length < vehicleCount) {
							trackedVehicles.push(vehicle);
						}
					});
				}

				// save current vehicles for next interval
				trackedInLastInterval = trackedVehicles.map(vehicle => vehicle['$'].id);
			} else {
				trackedVehicles = vehicles;
			}

      stepCallback(step, trackedVehicles.length);

			trackedVehicles.forEach(vehicle => {
				const parsedVehicle = parseVehicleData(vehicle, date);

				logger.debug(`At step ${step} ${parsedVehicle.carId} is at ${parsedVehicle.long} ${parsedVehicle.lat} doing ${vehicle['$'].speed}km/h. Radius is ${parsedVehicle.accuracy}m`);
				updateCallback(parsedVehicle);
			});
		}
	};
}

function parseVehicleData(vehicle, date) {
	const long = parseFloat(vehicle['$'].x);
	const lat = parseFloat(vehicle['$'].y);
	const location = randomizeLocation(long, lat, accuracy);

	return {
		carId: vehicle['$'].id,
		lon: location.long,
		lat: location.lat,
		accuracy: accuracy,
		timestamp: date,
	};
}