const ONE_DEGREE_IN_KM = 111.32 * 1000; // 1 decimal degree equals 111.32 km (https://en.wikipedia.org/wiki/Decimal_degrees)

/**
 * Generates a random location in a radius around a given location.
 * Idea taken from https://gist.github.com/mkhatib/5641004
 *
 * @param long
 * @param lat
 * @param radius
 * @returns {{long: *, lat: *}}
 */
function randomizeLocation(long, lat, radius) {
	// convert radius from meters to degrees.
	const rd = radius / ONE_DEGREE_IN_KM;

	const u = Math.random();
	const v = Math.random();

	const w = rd * Math.sqrt(u);
	const t = 2 * Math.PI * v;
	const x = w * Math.cos(t);
	const y = w * Math.sin(t);

	return {lat: y + lat, long: x + long};
}

module.exports = randomizeLocation;