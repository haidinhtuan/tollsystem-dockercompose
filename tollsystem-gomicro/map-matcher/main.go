package main


// Created by haidinhtuan on 27.11.2019

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	micro "github.com/micro/go-micro"
	nats "github.com/nats-io/nats.go"
	_ "github.com/micro/go-plugins/registry/consul"  // Indirect import is needed here since Consul has been made deprecated (https://github.com/micro/go-micro/issues/890)
)

var (
	natsURI            = os.Getenv("NATS_URI")
	osrmURI            = os.Getenv("OSRM_URI")
	subscribeQueueName = "location.update"
	publishQueueName   = "location.matched"
	logQueueName       = "logs"
	globalNatsConn     *nats.Conn
	messageQueue       = make(map[string][]SimulatorMessageData) // car id to locations dict; example id:locations:[.., .., .., ]
	messageQueueLength = 2
	vehicleWithMsg	   = make(map[string]SimulatorMessageData) // Map vehicle with Message
)


//SimulatorMessageData received by the Simulator
type SimulatorMessageData struct {
	MessageID int     `json:"messageId"`
	Framework string     `json:"framework"`
	CarID     string  `json:"carId"`
	Timestamp string  `json:"timestamp"`
	Accuracy  float64 `json:"accuracy"`
	Lat       float64 `json:"lat,float64"`
	Lon       float64 `json:"lon,float64"`
}

func (s SimulatorMessageData) toString() string {
	return fmt.Sprintf("%+v\n", s)
}

//SimulatorMessage received from the simulator queue
type SimulatorMessage struct {
	Event string               `json:"event"`
	Data  SimulatorMessageData `json:"data"`
}

func (s SimulatorMessage) toString() string {
	return fmt.Sprintf("%+v\n", s)
}

//LogMessage to be put into the nats log queue
type LogMessage struct {
	Data LogMessageData `json:"data"`
}

func (l LogMessage) toString() string {
	return fmt.Sprintf("%+v\n", l)
}

//LogMessageData to be put into the LogMessage
type LogMessageData struct {
	MessageID int    `json:"messageId"`
	Sender    string `json:"sender"`
	Framework string `json:"framework"`
	Type      string `json:"type"`
	Timestamp string `json:"timestamp"`
}

// MapMatcherOutput message struct
type MapMatcherOutput struct {
	Data MapMatcherMessage `json:"data"`
}

func (m MapMatcherOutput) toString() string {
	return fmt.Sprintf("%+v\n", m)
}

// MapMatcherMessage struct
type MapMatcherMessage struct {
	Framework string 				`json:"framework"`
	MessageID int           `json:"messageId"`
	CarID     string        `json:"carId"`
	Timestamp string        `json:"timestamp"`
	Route     []Coordinates `json:"route"`
	Sender    string        `json:"sender"`
	Topic     string        `json:"topic"`
}

func (m MapMatcherMessage) toString() string {
	return fmt.Sprintf("%+v\n", m)
}

//Coordinates Struct to unite a Latitude and Longitude to one location
type Coordinates struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

// OSRMResponse from the OSRM server
type OSRMResponse struct {
	Code        string
	Tracepoints []Tracepoints
}

// Tracepoints <meaningful comment>
type Tracepoints struct {
	AternativesCount int
	Location         []float64
	Distance         float64
	Hint             string
	Name             string
	MatchinIndex     int
	WaypointIndex    int
}

func (r OSRMResponse) toString() string {
	return fmt.Sprintf("%+v\n", r)
}

func pushToMessageQueue(ms SimulatorMessageData) {
	//fmt.Println("pushToMessageQueue called at ", time.Now())
	//messageQueue[ms.CarID] = append(messageQueue[ms.CarID], ms)

	//fmt.Println("current messageQueue length ", len(messageQueue[ms.CarID]))

	//if len(messageQueue[ms.CarID]) == messageQueueLength {
	//	fmt.Println("Condition MATCHED ")
	//
	//	msg1 := messageQueue[ms.CarID][len(messageQueue[ms.CarID])-1]
	//	msg2 := messageQueue[ms.CarID][len(messageQueue[ms.CarID])-2]
	//	messageQueue[ms.CarID] = messageQueue[ms.CarID][:len(messageQueue[ms.CarID])-1]
	//	messageQueue[ms.CarID] = messageQueue[ms.CarID][:len(messageQueue[ms.CarID])-1]
	//	go processMessage(msg1, msg2)
	//}

	if val, ok := vehicleWithMsg[ms.CarID]; ok {
		fmt.Println("Vehicle exists! ", val.CarID)
		go processMessage(ms,vehicleWithMsg[ms.CarID])
	} else {
		fmt.Println("Vehicle doesn't exists! ")
		vehicleWithMsg[ms.CarID] = ms
	}



}

func main() {
	service := micro.NewService(
		micro.Name("mapmatcher"),
		micro.RegisterTTL(time.Second*30),
		micro.RegisterInterval(time.Second*10),
	)

	// optionally setup command line usage
	service.Init()
	nc, _ := nats.Connect(natsURI)

	globalNatsConn = nc

	nc.Subscribe(subscribeQueueName, func(m *nats.Msg) {
		go subscribeHandler(m) //use go keyword here to use goroutines, i.e. put the function in a new thread
	})

	// Run server
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func subscribeHandler(m *nats.Msg) {
	fmt.Println("---Received a message:---\n", string(m.Data))
	fmt.Println("Message received at ", time.Now())
	var msg SimulatorMessage
	rawJSONMsg := json.RawMessage(m.Data)
	bytes, err := rawJSONMsg.MarshalJSON()
	if err != nil {
		fmt.Println(err)
	}
	err2 := json.Unmarshal(bytes, &msg)
	if err2 != nil {
		fmt.Println("error:", err)
	}

	go logMessage(msg.Data.MessageID, "received")
	go pushToMessageQueue(msg.Data)
}

func logMessage(MessageID int, msgType string) {
	fmt.Println("logMessage called at ", time.Now(), " message type ", msgType)
	RFC3339Milli := "2006-01-02T15:04:05.000Z07:00"
	msg := LogMessage{
		Data: LogMessageData{
			MessageID: MessageID,
			Sender:    "map-matcher",
			Framework: "gomicro",
			Type:      msgType,
			Timestamp: time.Now().Local().Format(RFC3339Milli),
		},
	}

	logOutput, err := json.Marshal(msg)
	if err != nil {
		log.Fatal(err)
	}

	globalNatsConn.Publish(logQueueName, logOutput)
	fmt.Printf("--- Message logged in queue %s ---\n", logQueueName)
	fmt.Println(string(logOutput))
}

func processMessage(msg1 SimulatorMessageData, msg2 SimulatorMessageData) {
	fmt.Println("processMessage called at ", time.Now())
	fmt.Println("----MESSAGE1----")
	fmt.Println(msg1.toString())
	fmt.Println("----MESSAGE2----")
	fmt.Println(msg2.toString())
	fmt.Println("---sending Data to osrm---")
	fmt.Println("Send request to OSRM at ", time.Now())
	client := &http.Client{}
	requestUrl := "http://" + osrmURI + "/match/v1/car/" + strconv.FormatFloat(msg1.Lon, 'f', -1, 64) + "," + strconv.FormatFloat(msg1.Lat, 'f', -1, 64) + ";" + strconv.FormatFloat(msg2.Lon, 'f', -1, 64) + "," + strconv.FormatFloat(msg2.Lat, 'f', -1, 64)
	req, _ := http.NewRequest("GET", requestUrl, nil)
	//req.Header.Set("Connection","Keep-Alive")
	//req.Header.Set("Accept-Language","en-US")
	//req.Header.Set("User-Agent","Mozilla/5.0")
	//req.Header.Set("Accept", "application/json")
	fmt.Println(requestUrl)

	resp, err := client.Do(req)

	if err != nil {
		fmt.Printf("--- OSRM error!----\n")
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var osrmRes OSRMResponse
	rawJSON := json.RawMessage(body)
	bytes, err := rawJSON.MarshalJSON()
	if err != nil {
		fmt.Println("---error:---\n", err)
		return
	}
	err2 := json.Unmarshal(bytes, &osrmRes)
	if err2 != nil {
		fmt.Println("---error:---\n", err)
		return
	}

	fmt.Println("Received response from OSRM at ", time.Now())


	fmt.Printf("--- OSRM output----\n")
	fmt.Println(osrmRes.toString())
	//var noMatchString = "NoMatch Tracepoints"
	//if !strings.Contains(osrmRes.toString(), noMatchString)  {
		const RFC3339Milli = "2006-01-02T15:04:05.999Z"
		msgData := MapMatcherMessage{
			Framework: "gomicro",
			Sender:    "map-matcher",
			Topic:     "location.matched",
			MessageID: msg1.MessageID,
			CarID:     msg1.CarID,
			Timestamp: time.Now().Local().Format(RFC3339Milli),
			Route: []Coordinates{
				Coordinates{
					Lat: osrmRes.Tracepoints[0].Location[1],
					Lon: osrmRes.Tracepoints[0].Location[0],
				},
				Coordinates{
					Lat: osrmRes.Tracepoints[1].Location[1],
					Lon: osrmRes.Tracepoints[1].Location[0],
				},
			},
		}

		mmOutput := MapMatcherOutput{
			Data: msgData,
		}

		publishMapMatcherMessage(mmOutput)
	//}
}

func publishMapMatcherMessage(msg MapMatcherOutput) {
	mmOutput, err := json.Marshal(msg)
	if err != nil {
		log.Fatal(err)
	}

	go globalNatsConn.Publish(publishQueueName, mmOutput)
	go logMessage(msg.Data.MessageID, "sent")
	fmt.Println("---published message---\n" + msg.toString())
	fmt.Println("--- Publishing process completed --- \n")
}
