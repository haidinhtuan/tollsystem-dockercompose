module github.com/haidinhtuan/map_matcher

go 1.13

require (
	github.com/ddliu/go-httpclient v0.6.3
	github.com/micro/examples v0.2.0 // indirect
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-plugins v1.5.1
	github.com/nats-io/nats.go v1.9.1
)
