"use strict";

const { ServiceBroker } = require("moleculer");
const Transporter = require("../transporter/nats-custom-transporter");

const broker = new ServiceBroker({
	transporter: new Transporter({
		url: "http://localhost:4222",
		user: "user",
		pass: "pass",
		name: "MapMatcherService2"
	}),
	logLevel: "debug",
	requestTimeout: 5 * 1000
});

broker.createService({
	name: "MapMatcherService2",
	events: {
		"location.update": {
			group: "toll-simulator",
			handler(payload) {
				broker.logger.info("Message received! ", payload);
				let parsedMsg = JSON.parse(JSON.stringify(payload));

				broker.logger.info("Received new message for ", parsedMsg.carId);
				broker.emit("location.matched", payload, "toll-simulator");
				broker.logger.info("Emitted!");

				//this.sendLogs(parsedMsg.messageId, "received");

				//this.buffer.pushPosition(parsedMsg.data);
			}
		}
	},

});

//natsBroker.start().then(() => natsBroker.logger.info("MapMatcher started!"));
//broker.start();

