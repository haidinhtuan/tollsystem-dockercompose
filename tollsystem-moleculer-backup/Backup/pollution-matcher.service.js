"use strict";

const { ServiceBroker } = require("moleculer");
const Transporter = require("../transporter/nats-custom-transporter");
const axios = require("axios");

const broker = new ServiceBroker({
	transporter: new Transporter({
		url: "http://localhost:4222",
		user: "user",
		pass: "pass",
		name: "PollutionMatcherService"
	}),
	logLevel: "debug",
	requestTimeout: 5 * 1000
});

broker.createService({
	name: "messageHandler",
	events: {
		"location.matched": {
			group: "map-matcher",
			handler(payload) {
				broker.logger.info("Message received! ", payload);
				let parsedMsg = JSON.parse(JSON.stringify(payload));

				broker.logger.info("Received new message for ", parsedMsg.carId);
				broker.emit("pollution.matched", payload, null);

				//this.sendLogs(parsedMsg.messageId, "received");

				//this.buffer.pushPosition(parsedMsg.data);
			}
		}
	},

});

module.exports = class Buffer {

	constructor() {
		this.cars = [];
		this.lats = [];
		this.longs = [];
	}

	pushPosition(data) {
		if (this.cars.indexOf(data.carId) != -1) {
			let carIdx = this.cars.indexOf(data.carId);
			this.lats[carIdx].push(data.lat);
			this.longs[carIdx].push(data.lon);

		} else {
			this.cars.push(data.carId);
			let idx = this.cars.indexOf(data.carId);
			this.lats[idx] = [data.lat];
			this.longs[idx] = [data.lon];

		}

		let idx = this.cars.indexOf(data.carId);
		if (this.lats[idx].length === 2 && this.longs[idx].length === 2) {
			let lats = this.lats[idx].slice(0, 2);
			let longs = this.longs[idx].slice(0, 2);
			this.lats[idx].shift();
			this.longs[idx].shift();
			console.log("data.accuracy: ", data.accuracy);
			this.osmMapMatcher(lats, longs, data.carId, data.messageId, function (err, result) {
				if (err) {
					console.log("failed!");
				}
				else {
					console.log("publishing to polution matcher", JSON.stringify(result));
					this.publishMapMatcherData(result, function (err, callback) {
						if (err) {
							console.log("an error has occurred!", err);
						}
						else {
							console.log("successfully pushed to pollution matcher!");
						}
					});

				}
			});

		}
	}

};
//natsBroker.start().then(() => natsBroker.logger.info("MapMatcher started!"));
broker.start();

