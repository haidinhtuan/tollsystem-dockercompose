"use strict";

const { ServiceBroker } = require("moleculer");
//const HTTPClientService = require("moleculer-http-client");
//const Transporter = require("../transporter/nats-transporter");

const broker = new ServiceBroker({
	transporter: {
		type: "NATS",
		options: {
			url: "http://localhost:4222",
			user: "user",
			pass: "pass",
			name: "Receiver"
		}
	},
	logLevel: "debug",
	requestTimeout: 5 * 1000,
	started() {
		broker.logger.info("Receiver started!");
	}
});

broker.createService({
	name: "receiver",
	events: {
		"location.update"(payload) {
			broker.logger.info("Message received! ", payload);
			// Do something
		}
	}
});

module.exports = {
};
broker.start();
//natsBroker.repl();

