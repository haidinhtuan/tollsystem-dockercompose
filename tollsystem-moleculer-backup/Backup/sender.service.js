"use strict";

const { ServiceBroker } = require("moleculer");
//const HTTPClientService = require("moleculer-http-client");
//const Transporter = require("../transporter/nats-transporter");

let broker = new ServiceBroker({
	transporter: {
		type: "NATS",
		options: {
			url: "http://localhost:4222",
			user: "user",
			pass: "pass",
			name: "Sender"
		}
	},
	logLevel: "debug",
	requestTimeout: 5 * 1000,
});

module.exports = {
	name: "sender",
	started() {
		this.counter = 1;
		this.logger.info("Broker started!");
		//natsBroker.emit("location.update", "String");
		setInterval(() => {
			this.logger.info(`>> Send location.udate event. Counter: ${this.counter}.`);
			this.broker.emit("location.update", { counter: this.counter++ });
		}, 1000);
	}
};
//natsBroker.start().then(() => natsBroker.logger.info("MapMatcher started!"));
//natsBroker.start();
//natsBroker.repl();

