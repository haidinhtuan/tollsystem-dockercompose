"use strict";

const { ServiceBroker } = require("moleculer");
//const HTTPClientService = require("moleculer-http-client");
//const Transporter = require("../transporter/nats-transporter");

let broker = new ServiceBroker({
	transporter: {
		type: "NATS",
		options: {
			url: "http://localhost:4222",
			user: "user",
			pass: "pass",
			name: "Sender"
		}
	},
	logLevel: "debug",
	requestTimeout: 5 * 1000,
});

broker.createService({
	name: "event-handler",
	started() {
		this.counter = 1;
		broker.logger.info("Broker started!");
		//natsBroker.emit("location.update", "String");
		setInterval(() => {
			broker.logger.info(`>> Send location.udate event. Counter: ${this.counter}.`);
			broker.emit("location.update", { counter: this.counter++ });
		}, 1000);
	}
});

module.exports = {
	name: "sender",
};
//natsBroker.start().then(() => natsBroker.logger.info("MapMatcher started!"));
//natsBroker.start();
//natsBroker.repl();
broker.start();

