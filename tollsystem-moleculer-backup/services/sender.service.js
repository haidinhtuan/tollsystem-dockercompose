"use strict";

//const HTTPClientService = require("moleculer-http-client");
//const Transporter = require("../transporter/nats-transporter");


module.exports = {
	name: "SenderService",
	settings: {
		sender: "sender",
		framework: "moleculer",
	},
	// events: {
	// 	"location.update": {
	// 		//group: "toll-simulator",
	// 		handler(payload) {
	// 			this.logger.info("Message received! by Sender", payload);
	// 			//let parsedMsg = JSON.parse(JSON.stringify(payload));
	//
	// 			//this.logger.info("Received new message for ", parsedMsg.carId);
	// 			//this.broker.broadcast("location.matched", payload, this.settings.sender);
	// 			//this.logger.info("Emitted!");
	//
	// 			//this.sendLogs(parsedMsg.messageId, "received");
	//
	// 			//this.buffer.pushPosition(parsedMsg.data);
	// 		}
	// 	}
	// },
	started() {
		this.counter = 1;
		this.broker.logger.info("Broker started!");
		//natsBroker.emit("location.update", "String");
		setInterval(() => {
			this.broker.logger.info(`>> Send location.update event. Counter: ${this.counter}.`);
			this.broker.emit("location.update", { counter: this.counter++ });
			//this.broker.emit("location.update", { counter: this.counter++ }, this.settings.sender);
		}, 1000);
	}
};