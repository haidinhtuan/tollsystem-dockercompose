package com.haidinhtuan.mapmatcher;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;

import com.google.gson.*;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import io.nats.client.Dispatcher;
import io.nats.client.Connection;
import io.nats.client.Nats;

import static java.lang.System.out;

@RestController
@EnableAutoConfiguration
public class MapMatcher {
	private static RouteHandler routeHandler;
	public static void main(String[] args) throws Exception {
		SpringApplication.run(MapMatcher.class, args);

		routeHandler = new RouteHandler();

		String natsUrl = System.getenv().get("NATS_URL");

		Connection nats = Nats.connect(natsUrl);

		String locationsChannel = System.getenv().get("CHANNEL_INPUT");
		String routesChannel = System.getenv().get("CHANNEL_OUTPUT");
		String logChannel = System.getenv().get("CHANNEL_LOGS");

		// Use a latch to wait for a message to arrive
		CountDownLatch latch = new CountDownLatch(100);
		JsonParser parser = new JsonParser();

		// Create a dispatcher and inline message handler
		Dispatcher d = nats.createDispatcher((msg) -> {
			String sumoMessage = new String(msg.getData(), StandardCharsets.UTF_8);
			JsonObject event = parser.parse(sumoMessage).getAsJsonObject();
			JsonObject vehicleInfo = event.get("data").getAsJsonObject();

			// input log
			try {
				nats.publish(logChannel, getLog(event, "received").toString().getBytes(StandardCharsets.UTF_8));
			} catch (Exception e) {
				out.println("logging problem :/");
			}

			JsonObject route = routeHandler.getRoute(vehicleInfo);

			
			// send route to pollutionMatcher
			if (route != null && route.get("data").getAsJsonObject().get("route").getAsJsonArray().size() != 0) {
				nats.publish(routesChannel, route.toString().getBytes(StandardCharsets.UTF_8));
//				try {
//					nats.flush(Duration.ZERO);
//				} catch (TimeoutException e) {
//					e.printStackTrace();
//				}

				// output log
				nats.publish(logChannel, getLog(route, "sent").toString().getBytes(StandardCharsets.UTF_8));
//				try {
//					nats.flush(Duration.ZERO);
//				} catch (TimeoutException e) {
//					e.printStackTrace();
//				}
			} else {
				out.println("empty route, not sending anything.");
			}

		});

		d.subscribe(locationsChannel);

		System.out.println(" Subscribed to " + locationsChannel);
		latch.await();

	}

	private static JsonObject getLog(JsonObject event, String type) {
		/*
			{
				"data": {
					"sender": "map-matcher",
					"framework": "gomicro",
					"type": "received", // "received" or "sent"
					"messageId": 123,
					"timestamp": "2019-01-10T10:42:18.189Z", // ISO 8601
				}
			}
			*/
		JsonObject logData = new JsonObject();
		logData.addProperty("sender","map-matcher");
		logData.addProperty("framework","spring-boot");
		logData.addProperty("type", type);
		logData.add("messageId", event.get("data").getAsJsonObject().get("messageId"));
		logData.addProperty("timestamp", routeHandler.getTimestamp());

		JsonObject data = new JsonObject();
		data.add("data", logData);
		return data;
	}


}
