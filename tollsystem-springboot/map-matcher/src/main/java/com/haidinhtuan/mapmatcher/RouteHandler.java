package com.haidinhtuan.mapmatcher;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

class RouteHandler {
    private Map<String, List<JsonObject>> lastLocations;

    RouteHandler(){
        this.lastLocations = new HashMap<>();
    }

    JsonObject getRoute(JsonObject vehicleInfo) {
        // get last location
        String carId = vehicleInfo.get("carId").getAsString();
        Vector<JsonObject> coordinates = new Vector<>();

        if (lastLocations.get(carId) != null) {
            JsonObject lastLocation = lastLocations.get(carId).get(lastLocations.get(carId).size() - 1);
//            System.out.println("last locations for " + carId + " : " + lastLocations.get(carId));
            coordinates.add(lastLocation);
        } else {
            lastLocations.put(carId, new ArrayList<>());
        }

        JsonObject newLocation = new JsonObject();
        newLocation.addProperty("carId", carId);
        newLocation.addProperty("latitude",vehicleInfo.get("lat").getAsDouble());
        newLocation.addProperty("longitude",vehicleInfo.get("lon").getAsDouble());
        coordinates.add(newLocation);
        lastLocations.get(carId).add(newLocation);

        // message creation
        JsonObject message = new JsonObject();
        message.addProperty("sender", "map-matcher");
        message.addProperty("framework", "spring-boot");
        message.addProperty("topic", "location.matched");
        message.addProperty("timestamp", getTimestamp());
        message.add("messageId", vehicleInfo.get("messageId"));
        message.add("carId", vehicleInfo.get("carId"));

        if (coordinates.size() >= 2) {
            message.add("route", getMatchedRoute(coordinates));
        } else {
            message.add("route", new JsonArray());
        }

        JsonObject data = new JsonObject();
        data.add("data", message);
        return data;
    }

    String getTimestamp() {
        Instant instant = Instant.now(); // Current date-time in UTC.
        Instant instant2 = instant.truncatedTo( ChronoUnit.MILLIS ) ;
        return instant2.toString();
    }

    private JsonArray getMatchedRoute(Vector<JsonObject> coordinatesList) {

        String coordinatesString = buildString(coordinatesList);

        // ask osrm for map matching
        String osrmUrl = System.getenv("OSRM_URL");
        String url = String.format(osrmUrl + "/match/v1/driving/%s?overview=false", coordinatesString);
        HttpClient httpClient = HttpClients.createDefault();
        JsonObject result = null;
        try
        {
            URIBuilder builder = new URIBuilder(url);
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            request.addHeader("accept", "application/json");
            HttpResponse response = httpClient.execute(request);
            JsonParser parser = new JsonParser();
            result = parser.parse(IOUtils.toString(response.getEntity().getContent())).getAsJsonObject();
        }
        catch (Exception e)
        {
            System.out.println("request to osrm failed :");
            e.printStackTrace();
        }

        // get json coordinates
        JsonArray tracepoints;
        JsonArray matchedRoute = null;
        try {
            assert result != null;
            tracepoints = result
                    .get("tracepoints").getAsJsonArray();
            matchedRoute = new JsonArray();
            for (JsonElement o : tracepoints) {
                JsonObject coordinates = new JsonObject();
                coordinates.add("lat", o.getAsJsonObject().get("location").getAsJsonArray().get(1));
                coordinates.add("lon", o.getAsJsonObject().get("location").getAsJsonArray().get(0));
                matchedRoute.add(coordinates);
            }
        } catch (Exception e) {
            System.out.println("Json build failed :");
            e.printStackTrace();
        }
        return matchedRoute;
    }

    private String buildString(Vector<JsonObject> coordinatesList) {
        StringBuilder coordinatesString = new StringBuilder();
        for (JsonObject coordinates : coordinatesList) {
            coordinatesString.append(coordinates.get("longitude").getAsDouble()).append(",").append(coordinates.get("latitude").getAsDouble()).append(";");
        }
        // remove last ';'
        coordinatesString = new StringBuilder(coordinatesString.substring(0, coordinatesString.length() - 1));

        return coordinatesString.toString();
    }
}
