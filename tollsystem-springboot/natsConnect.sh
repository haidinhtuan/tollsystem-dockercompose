#!/usr/bin/expect

set timeout 180
set port 4222
set host "localhost"

#put as arg the channel you want to connect to
set channel [lindex $argv 0]

spawn telnet $host $port
sleep 0.1
send "sub $channel 90\n"

interact