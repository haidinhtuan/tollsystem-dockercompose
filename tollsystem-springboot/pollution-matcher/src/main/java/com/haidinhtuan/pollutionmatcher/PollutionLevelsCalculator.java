package com.haidinhtuan.pollutionmatcher;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static java.lang.System.out;


class PollutionLevelsCalculator {
    JsonArray getSegments(JsonArray route) {
        JsonArray segments = new JsonArray();

        if (route.size() == 0) { return segments; }

        // build the linestring query object
        String lineString = "'LINESTRING(";
        for (int i=0;i<route.size();i++) {
            JsonObject coordinates = route.get(i).getAsJsonObject();
            double y = coordinates.get("lat").getAsDouble();
            double x = coordinates.get("lon").getAsDouble();
            lineString += x + " " + y + ",";
        }
        lineString = lineString.substring(0, lineString.length() - 1);
        lineString += ")'";
//        out.println("Coordinates : " + lineString);

        // query database
        Connection c;
        Statement stmt;
        try {

            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://postgis:5432/gis",
                            "docker", "docker");
            c.setAutoCommit(false);
//            System.out.println("Opened database successfully");
            stmt = c.createStatement();

            String query =
                    "SELECT ST_AsGeoJson(ST_intersection (a.outline, ST_GeomFromText("+lineString+", 4326))) as geometry, a.pollution " +
                    "FROM berlin_polygons a " +
                    "WHERE not ST_IsEmpty(ST_AsText(ST_intersection (a.outline, ST_GeomFromText("+lineString+",4326))))";
//            out.println("---------------------- query  ---------------- \n" + query);
//            long t1 = System.currentTimeMillis();
            ResultSet rs = stmt.executeQuery(query);
//            long t2 = System.currentTimeMillis();
//            out.println("time spent in PCalc : " + (t2 - t1));
            JsonParser parser = new JsonParser();
            int segmentId = 0;
            while ( rs.next() ) {
                JsonObject segment = new JsonObject();
                segment.addProperty("segmentId", segmentId);
                segmentId += 1;
                JsonArray segmentSections = parser.parse(rs.getString("geometry")).getAsJsonObject().get("coordinates").getAsJsonArray();
                JsonArray newSegmentSections = new JsonArray();
                segmentSections.forEach(item -> {
                    JsonObject coordinates = new JsonObject();
                    coordinates.add("lat",item.getAsJsonArray().get(0));
                    coordinates.add("lon",item.getAsJsonArray().get(1));
                    newSegmentSections.add(coordinates);
                });
                int pollutionLevel = Integer.valueOf(rs.getString("pollution"));
                segment.addProperty("pollutionLevel", pollutionLevel);
                segment.add("segmentSections", newSegmentSections);
                segments.add(segment);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            out.println("problem with database.");
            e.printStackTrace();
            return new JsonArray();
        }

        return segments;
    }
}
