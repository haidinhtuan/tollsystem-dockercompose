package com.haidinhtuan.pollutionmatcher;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RestController;

import io.nats.client.Dispatcher;
import io.nats.client.Connection;
import io.nats.client.Nats;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;

@RestController
@EnableAutoConfiguration
public class PollutionMatcher {

	private static PollutionHandler handler;
	public static void main(String[] args) throws Exception
	{
		SpringApplication.run(PollutionMatcher.class, args);

		String natsUrl = System.getenv().get("NATS_URL");
		Connection nats = Nats.connect(natsUrl);

		handler = new PollutionHandler(nats);

		String routesChannel = System.getenv().get("CHANNEL_INPUT");
		String logChannel = System.getenv().get("CHANNEL_LOGS");

		CountDownLatch latch = new CountDownLatch(100);
		JsonParser parser = new JsonParser();

		// Create a dispatcher and inline message handler
		Dispatcher d = nats.createDispatcher((msg) -> {
			// receive message from map matcher
			String mapMatcherMsg = new String(msg.getData(), StandardCharsets.UTF_8);
			JsonObject routeMsg = parser.parse(mapMatcherMsg).getAsJsonObject();

			new Thread(() -> new PollutionHandler(nats).getPollutionLevels(routeMsg)).start();
//			JsonObject pollutionLevels = handler.getPollutionLevels(routeMsg);

//			System.out.println("sent to toll calculator : " + pollutionLevels);

		});

		d.subscribe(routesChannel);
		System.out.println("Subscribed to : " + routesChannel);

		latch.await();
	}


	
	
}