package com.haidinhtuan.pollutionmatcher;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.nats.client.Connection;

import static java.lang.System.out;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

class PollutionHandler  {
    private PollutionLevelsCalculator polcalc;
    private Connection nats;
    private String pollutionChannel;
    private String logChannel;

    PollutionHandler(Connection nats) {
        polcalc = new PollutionLevelsCalculator();
        this.nats = nats;
        pollutionChannel = System.getenv().get("CHANNEL_OUTPUT");
        logChannel = System.getenv().get("CHANNEL_LOGS");
    }

    void getPollutionLevels(JsonObject routeMsg) {
        JsonObject routeInfo = null;
        JsonArray route = null;
        JsonArray segments = null;

        try {
            nats.publish(logChannel, getLog(routeMsg, "received").toString().getBytes(StandardCharsets.UTF_8));
//            nats.flush(Duration.ZERO);
        } catch (Exception e) {
            out.println("logging problem :/");
        }

        try {
            routeInfo = routeMsg.getAsJsonObject("data");
            route = routeInfo.getAsJsonArray("route");
//            long t1 = System.currentTimeMillis();
            segments = polcalc.getSegments(route);
//            long t2 = System.currentTimeMillis();
//            out.println("time spent in PCalc : " + (t2 - t1));
//            System.out.println("segments to process : " + segments);
        } catch (Exception e) {
            out.println("Problem with pollution matching : " + e.getMessage());
        }

        JsonObject data = new JsonObject();
        data.addProperty("sender", "pollution-matcher");
        data.addProperty("framework", "spring-boot");
        data.addProperty("topic", "pollution.matched");
        data.addProperty("timestamp", getTimestamp());
        data.add("messageId", routeInfo.get("messageId"));
        data.add("carId", routeInfo.get("carId"));
        data.add("segments", segments);

        if (segments == null) {
            out.print("segments null, carId : " + routeInfo.get("carId"));
            out.println(", route : " + route);
        } else if (segments.size() == 0) {
            out.print("segments empty, carId : " + routeInfo.get("carId"));
            out.println(", route : " + route);
        } else {
//            out.println("segments ok :D");
        }

        JsonObject pollutionLevels = new JsonObject();
        pollutionLevels.add("data", data);

        nats.publish(pollutionChannel, pollutionLevels.toString().getBytes(StandardCharsets.UTF_8));

        // output log
        nats.publish(logChannel, getLog(pollutionLevels, "sent").toString().getBytes(StandardCharsets.UTF_8));
    }


    private String getTimestamp() {
        Instant instant = Instant.now(); // Current date-time in UTC.
        Instant instant2 = instant.truncatedTo( ChronoUnit.MILLIS ) ;
        return instant2.toString();
    }

    private JsonObject getLog(JsonObject event, String type) {
		/*
			{
				"data": {
					"sender": "map-matcher",
					"framework": "gomicro",
					"type": "received", // "received" or "sent"
					"messageId": 123,
					"timestamp": "2019-01-10T10:42:18.189Z", // ISO 8601
				}
			}
			*/
        JsonObject logData = new JsonObject();
        logData.addProperty("sender","pollution-matcher");
        logData.addProperty("framework","spring-boot");
        logData.addProperty("type", type);
        logData.add("messageId", event.get("data").getAsJsonObject().get("messageId"));
        logData.addProperty("timestamp", getTimestamp());

        JsonObject data = new JsonObject();
        data.add("data", logData);
        return data;
    }
}
