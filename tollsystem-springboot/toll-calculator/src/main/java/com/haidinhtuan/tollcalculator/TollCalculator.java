package com.haidinhtuan.tollcalculator;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RestController;

import io.nats.client.Dispatcher;
import io.nats.client.Connection;
import io.nats.client.Nats;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;

import static java.lang.System.out;

@RestController
@EnableAutoConfiguration
public class TollCalculator {

	private static TollHandler handler;

	public static void main(String[] args) throws Exception
	{
		SpringApplication.run(TollCalculator.class, args);
		handler = new TollHandler();

		String natsUrl = System.getenv().get("NATS_URL");
		Connection nats = Nats.connect(natsUrl);

		String pollutionChannel = System.getenv().get("CHANNEL_INPUT");
		String outputChannel = System.getenv().get("CHANNEL_OUTPUT");
		String logChannel = System.getenv().get("CHANNEL_LOGS");

		CountDownLatch latch = new CountDownLatch(100);

		// Create a dispatcher and inline message handler
		Dispatcher d = nats.createDispatcher((msg) -> {
			// receive message from pollution matcher
			String pollutionMatcherMsg = new String(msg.getData(), StandardCharsets.UTF_8);
//			out.println("received from pollution matcher : " + pollutionMatcherMsg);
			JsonParser parser = new JsonParser();
			JsonObject event = parser.parse(pollutionMatcherMsg).getAsJsonObject();

			// input log
			try {
//				out.println("log received : " + getLog(event, "received"));
				nats.publish(logChannel, getLog(event, "received").toString().getBytes(StandardCharsets.UTF_8));
			} catch (Exception e) {
				out.println("logging problem :/");
			}

			JsonObject pollutionInfo = event.get("data").getAsJsonObject();
			JsonObject toll = handler.getToll(pollutionInfo);
//			if (pollutionInfo.get("carId").getAsString().equals("veh0")) {
//			out.println("toll sent : " + toll);
//			}
			try {
				nats.publish(outputChannel, toll.toString().getBytes(StandardCharsets.UTF_8));
				// output log
				nats.publish(logChannel, getLog(toll, "sent").toString().getBytes(StandardCharsets.UTF_8));
			} catch (Exception e) {
				out.println("problem with output");
				e.printStackTrace();
			}
		});

		d.subscribe(pollutionChannel);
		System.out.println("Subscribed to : " + pollutionChannel);

//		while (true) { latch<.await(); }
		latch.await();
		nats.close();

	}

	private static JsonObject getLog(JsonObject event, String type) {
		/*
			{
				"data": {
					"sender": "map-matcher",
					"framework": "gomicro",
					"type": "received", // "received" or "sent"
					"messageId": 123,
					"timestamp": "2019-01-10T10:42:18.189Z", // ISO 8601
				}
			}
			*/
//		out.println("event messageId : " + event.get("data").getAsJsonObject().get("messageId"));
		JsonObject logData = new JsonObject();
		logData.addProperty("sender","toll-calculator");
		logData.addProperty("framework","spring-boot");
		logData.addProperty("type", type);
		try {
			logData.add("messageId", event.get("data").getAsJsonObject().get("messageId"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			logData.addProperty("timestamp", handler.getTimestamp());
		} catch (Exception e) {
			e.printStackTrace();
		}


		JsonObject data = new JsonObject();
		data.add("data", logData);
		return data;
	}
	
	
}