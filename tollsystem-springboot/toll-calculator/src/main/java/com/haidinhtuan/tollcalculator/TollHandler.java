package com.haidinhtuan.tollcalculator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import static java.lang.System.out;

class TollHandler {
    private Map<Integer, Double> tollLevels;
    private Map<String, Double> vehicleTolls;

    TollHandler() {
        this.tollLevels = new HashMap<>();
        tollLevels.put(1, 0.1);
        tollLevels.put(2, 0.2);
        tollLevels.put(3, 0.3);
        tollLevels.put(4, 0.4);
        tollLevels.put(5, 0.5);
        tollLevels.put(6, 0.6);
        tollLevels.put(7, 0.7);
        tollLevels.put(8, 0.8);
        tollLevels.put(9, 0.9);
        tollLevels.put(10, 1.0);

        this.vehicleTolls = new HashMap<>();
    }

    JsonObject getToll(JsonObject message) {

        // get actual toll
        Double acualToll = 0D;
        JsonArray segments = message.get("segments").getAsJsonArray();
        for (JsonElement segment : segments) {
            try {
                acualToll += getSegmentToll(segment.getAsJsonObject());
            } catch (Exception e) {
                out.println("problem with toll calculation, removing toll");
            }

        }

        JsonObject tollObject = new JsonObject();

        // get total toll
        String carId = message.get("carId").getAsString();
        vehicleTolls.putIfAbsent(carId, 0D);
        Double previousToll = vehicleTolls.get(carId);
        acualToll += previousToll;
        vehicleTolls.put(carId, acualToll);
        tollObject.addProperty("toll",acualToll);
        tollObject.addProperty("framework","spring-boot");
        tollObject.addProperty("sender","toll-calculator");
        tollObject.addProperty("topic","toll.calculated");
        tollObject.add("messageId",message.get("messageId"));
        tollObject.addProperty("timestamp",getTimestamp());
        tollObject.add("carId", message.get("carId"));

        JsonObject data = new JsonObject();
        data.add("data", tollObject);
        return data;
    }

    private Double getSegmentToll(JsonObject segment) {
        JsonArray coordinates = null;
        int pollutionLevel;
        double toll = 0;
        Double tollPerKilometer = 0D;
//        out.println("segment : " + segment);
        try {
            coordinates = segment.get("segmentSections").getAsJsonArray();
            pollutionLevel = segment.get("pollutionLevel").getAsInt();
//            out.println("coordinates : " + coordinates);
//            out.println("pollution level : " + pollutionLevel);
            tollPerKilometer = tollLevels.get(pollutionLevel);
        } catch (Exception e) {
            out.println("problem with coordinates retrieval");
        }

        for (int i = 1;i < coordinates.size();i++) {
            JsonObject obj1 = coordinates.get(i-1).getAsJsonObject();
            JsonObject obj2 = coordinates.get(i).getAsJsonObject();
            toll += distance(obj1,obj2,'K') * tollPerKilometer;
        }

        return toll;
    }

    private double distance(JsonObject obj1, JsonObject obj2, char unit) {

        double lat1 = obj1.get("lat").getAsDouble();
        double lon1 = obj1.get("lon").getAsDouble();

        double lat2 = obj2.get("lat").getAsDouble();
        double lon2 = obj2.get("lon").getAsDouble();

        double theta = lon1 - lon2;

        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);

        dist = rad2deg(dist);

        dist = dist * 60 * 1.1515;

        if (unit == 'K') {

            dist = dist * 1.609344;

        } else if (unit == 'N') {

            dist = dist * 0.8684;

        } else if (unit == 'm') {

            dist = dist * 1.609344 * 1000.0;

        }

        return (dist);

    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    /*::  This function converts decimal degrees to radians             :*/

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    private double deg2rad(double deg) {

        return (deg * Math.PI / 180.0);

    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    /*::  This function converts radians to decimal degrees             :*/

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    private double rad2deg(double rad) {

        return (rad * 180.0 / Math.PI);

    }

    String getTimestamp() {
        Instant instant = Instant.now(); // Current date-time in UTC.
        Instant instant2 = instant.truncatedTo( ChronoUnit.MILLIS ) ;
        return instant2.toString();
    }
}
