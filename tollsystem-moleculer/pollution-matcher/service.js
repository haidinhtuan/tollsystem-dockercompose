"use strict";

const Pool = require("pg").Pool;
const pool = new Pool({
	user: "docker",
	host: "postgis",
	database: "gis",
	password: "docker",
	port: 5432,
});

module.exports = {
	name: "PollutionMatcherService",
	settings: {
		sender: "pollution-matcher",
		framework: "moleculer",
	},
	methods: {
		sendLog(messageId, type) {
			let payload = {};
			payload.sender = this.settings.sender;
			payload.framework = this.settings.framework;
			payload.type = type;
			payload.messageId = messageId;
			payload.timestamp = new Date().toISOString();

			this.broker.emit("logs", payload, "toll-simulator");
		},
		//TODO implement logic
		sendToTollCalculator(data) {
			this.logger.info("sendToTollCalculator received message ", JSON.stringify(data));
			let parsedData = JSON.parse(JSON.stringify(data));

			let payload = {};
			payload.sender = this.settings.sender;
			payload.framework = this.settings.framework;
			payload.messageId = parsedData.data.messageId;
			payload.carId = parsedData.data.carId;
			payload.timestamp = new Date().toISOString();
			payload.segments = parsedData.data.segments;
			payload.topic = "pollution.matched";


			this.broker.emit("pollution.matched", payload, this.settings.sender);
		},
		querryDatabase(route, payload, callback) {
			let geoJson = "ST_GeomFromText('LINESTRING(";
			console.log("\n Length of route: " + route.length);
			for (let i=0; i<route.length; i++) {
				console.log(route[i].lat);
				geoJson += route[i].lon;
				geoJson += " ";
				geoJson += route[i].lat;
				if(i+1 < route.length){
					geoJson += ", ";
				}

			}
			geoJson += ")',4326)";

			let selectQuery = "SELECT ST_AsGeoJson(ST_intersection (a.outline, " + geoJson + ")) as geometry, a.pollution, a.id FROM berlin_polygons a WHERE not ST_IsEmpty(ST_AsText(ST_intersection (a.outline, " + geoJson + ")))";

			pool.query(selectQuery, (error, results) => {
				if (error) {
					console.log(error);
					callback(error, null);
				} else {

					let segments = [];

					if(results.rowCount ===0) {
						//console.log ( "ZEEERO: " + selectQuery);
						//console.log("\nresult zero: " + JSON.stringify(results));
						console.info("No result from the database");
					} else {
						for(let idx in results.rows){
							let row = JSON.parse(results.rows[idx].geometry);
							let location = row.coordinates
							let s = {
								"segmentId": results.rows[idx].id,
								"pollutionLevel": results.rows[idx].pollution,
								"segmentSections": [
									{
										lat: location[0][0],
										lon: location[0][1]
									},
									{
										lat: location[1][0],
										lon: location[1][1]
									}
								]
							};

							segments.push(s);
							payload.data.segments = segments;
						};

					}
					callback(null, payload)

				};

			});
		},

	},
	events: {
		"location.matched": {
			group: "map-matcher",
			handler(payload) {
				let parsedMsg = JSON.parse(JSON.stringify(payload));
				this.sendLog(parsedMsg.messageId, "received");
				let payloadToTollCalculator =
					{
						data: {
							sender: this.settings.sender,
							topic: "pollution.matched",
							messageId : parsedMsg.messageId,
							carId: parsedMsg.carId,
							timestamp: new Date().toISOString(),
							segments : []
						}
					};

				this.querryDatabase(parsedMsg.route, payloadToTollCalculator, (err, data) => {
					if (err) {
						//Do nothing
					}
					else {
						this.sendToTollCalculator(data);
						this.sendLog(parsedMsg.messageId, "sent");
					}
				});
			}
		}
	},
	started() {
		this.logger.info("PollutionMatcherService started");
	},
};
