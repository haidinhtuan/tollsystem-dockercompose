"use strict";

module.exports = {
	name: "MapMatcherService",
	settings: {
		sender: "map-matcher",
		framework: "moleculer",
	},
	events: {
		"location.update": {
			//group: "toll-simulator",
			handler(payload) {
				this.logger.info("Message received! ", payload);
				//let parsedMsg = JSON.parse(JSON.stringify(payload));

				//this.logger.info("Received new message for ", parsedMsg.carId);
				//this.broker.broadcast("location.matched", payload, this.settings.sender);
				//this.logger.info("Emitted!");

				//this.sendLogs(parsedMsg.messageId, "received");

				//this.buffer.pushPosition(parsedMsg.data);
			}
		}
	},
	started() {
		this.logger.info("MapMatcherService started");
	}
};

//natsBroker.start().then(() => natsBroker.logger.info("MapMatcher started!"));
//broker.start();

