"use strict";

const haversine = require("haversine");

module.exports = {
	name: "TollCalculatorSvice",
	settings: {
		sender: "toll-calculator",
		framework: "moleculer",
	},
	methods: {
		sendLog(messageId, type) {
			let payload = {};
			payload.sender = this.settings.sender;
			payload.framework = this.settings.framework;
			payload.type = type;
			payload.messageId = messageId;
			payload.timestamp = new Date().toISOString();


			this.broker.emit("logs", payload, "toll-simulator");
		},
		sendToDashboard(data) {
			let parsedData = JSON.parse(JSON.stringify(data));
			let payload = {};
			payload.sender = this.settings.sender;
			payload.framework = this.settings.framework;
			payload.messageId = parsedData.messageId;
			payload.carId = parsedData.carId;
			payload.timestamp = new Date().toISOString();
			payload.toll = parsedData.toll;
			payload.topic = "toll.calculated";

			this.broker.emit("toll.calculated", payload, this.settings.sender);
		},
		calculateToll(payload, callback) {
			let currentToll = 0;
			payload.segments.forEach(element => {
				// eslint-disable-next-line no-useless-escape
				let removeChar = element.segmentSections;
				const firstCoordinates = {
					latitude: removeChar[0]['lat'],
					longitude: removeChar[0]['lon']
				};

				const secondCoordinates = {
					latitude: removeChar[1]['lat'],
					longitude: removeChar[1]['lon']
				};
				let haversineResult = haversine(firstCoordinates, secondCoordinates);

				let level = element.pollutionLevel / 100;

				currentToll += Math.ceil(haversineResult * level);

			});


			let previousToll = 0;

			if(this.vehicleToTollMap.has(payload.carId) === true) {
				previousToll = this.vehicleToTollMap.get(payload.carId);
			}
			let updatedToll = previousToll + currentToll;

			this.vehicleToTollMap.set(payload.carId, updatedToll);

			let tollCalculatorToDashboardMessage = {
				messageId: payload.messageId,
				carId: payload.carId,
				toll: this.vehicleToTollMap.get(payload.carId)
			};

			callback(null,tollCalculatorToDashboardMessage);
		}

	},
	events: {
		"pollution.matched": {
			group: "pollution-matcher",
			handler(payload) {
				let parsedMsg = JSON.parse(JSON.stringify(payload));
				this.sendLog(parsedMsg.messageId, "received");
				this.calculateToll(parsedMsg, (error, data) => {
					if (error) {
						//this.logger.info("failed!");
					}
					else {
						this.sendToDashboard(data);
						this.sendLog(parsedMsg.messageId, "sent");
					}
				});
			}
		}
	},
	started() {
		this.logger.info("TollCalculatorService started");
	},
	created() {
		this.vehicleToTollMap = new Map();
	}
};
