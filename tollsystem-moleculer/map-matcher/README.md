# map-matcher

## Build Setup

``` bash
# Install dependencies
npm install

# Start developing with REPL
npm run dev

# Start production
npm start

# Run ESLint
npm run lint
```

## Run in Docker

**Build Docker image**
```bash
$ docker build -t map-matcher .
```

**Start container**
```bash
$ docker run -d map-matcher
```
