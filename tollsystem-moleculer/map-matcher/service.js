"use strict";


const axios = require("axios");



module.exports = {
	name: "MapMatcherService",

	settings: {
		sender: "map-matcher",
		framework: "moleculer",
	},

	methods: {
		sendLog(messageId, type) {
			let payload = {};
			payload.sender = this.settings.sender;
			payload.framework = this.settings.framework;
			payload.type = type;
			payload.messageId = messageId;
			payload.timestamp = new Date().toISOString();


			this.broker.emit("logs", payload, this.settings.sender);


			//this.logger.info(JSON.stringify(payload));
		},
		sendToPollutionMatcher(data) {
			let parsedData = JSON.parse(JSON.stringify(data));

			let payload = {};
			payload.sender = this.settings.sender;
			payload.framework = this.settings.framework;
			payload.messageId = parsedData.data.messageId;
			payload.carId = parsedData.data.carId;
			payload.timestamp = parsedData.data.timeStamp;
			payload.route = parsedData.data.route;
			payload.topic = "location.matched";


			this.broker.emit("location.matched", payload, this.settings.sender);

		},
		querryOSRM(previousLon, previousLat, currentLon, currentLat, messageId, carId, callback) {

			let osrmURL = process.env.OSRM_URL + "/match/v1/car/";

			let finalURL = osrmURL + previousLon + "," + previousLat + ";" + currentLon + "," + currentLat + "?geometries=polyline";
			//this.logger.info("\n final url: " + finalURL);

			const payload = {};

			axios.get(finalURL)
				.then(function (response) {
					console.log("The response is:", response.data);
					payload.data = {
						sender: "map-matcher",
						topic: "location.matched",
						messageId: messageId,
						carId: carId,
						timeStamp: new Date().toISOString(),
						route: [
							{
								lat: response.data.tracepoints[0].location[1],
								lon: response.data.tracepoints[0].location[0]
							},
							{
								lat: response.data.tracepoints[1].location[1],
								lon: response.data.tracepoints[1].location[0]
							}]
					};

					callback(null, payload);
				})
				.catch(function (error) {
					console.log("for id: an error has occured:", error);
					callback(error, null);
				});
		},
	},
	events: {
		"location.update": {
			group: "toll-simulator",
			handler(payload) {
				let parsedMsg = JSON.parse(JSON.stringify(payload));
				this.sendLog(parsedMsg.messageId, "received");

				if(this.vehicleToMessageMap.has(parsedMsg.carId) === false) {
					this.vehicleToMessageMap.set(parsedMsg.carId, parsedMsg);
				} else {
					let previousLat = this.vehicleToMessageMap.get(parsedMsg.carId).lat;
					let previousLon = this.vehicleToMessageMap.get(parsedMsg.carId).lon;
					let currentLat = parsedMsg.lat;
					let currentLon = parsedMsg.lon;
					this.querryOSRM(previousLon, previousLat, currentLon, currentLat, parsedMsg.messageId, parsedMsg.carId, (err, data) => {
						if (err) {
							// Do nothing
						} else {
							this.sendToPollutionMatcher(data);
							this.sendLog(parsedMsg.messageId, "sent");
						}
					});
				}
			}
		}
	},
	started() {
		this.logger.info("MapMatcherService started");
	},
	created() {
		this.vehicleToMessageMap = new Map();
	}
};
