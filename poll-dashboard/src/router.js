import Vue from 'vue';
import Router from 'vue-router';
import Map from './views/Map.vue';
import Scope from './views/Scope.vue';
import Events from './views/Events.vue';
import Performance from './views/Performance.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Simulation',
      component: Map
    },
    {
      path: '/scope',
      name: 'Scope',
      component: Scope,
    },
    {
      path: '/events',
      name: 'Events',
      component: Events,
    },
    {
      path: '/performance',
      name: 'Performance',
      component: Performance,
    },
  ]
});
