# poll-dashboard

## Project setup
```
npm install
```

### Compiles and minifies for production
```
npm run build
```

### Start server
First build the server using the command above. Then run:
```
npm run start
```