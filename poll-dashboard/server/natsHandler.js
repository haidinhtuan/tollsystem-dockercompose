'use strict';

const throttle = require('lodash.throttle');
const URL = process.env.NATS_URL;
const NATS = require('nats');
const nats = NATS.connect({url: URL, json: true, name: 'poll-dashboard'});
const logger = require('./logger');
const sockets = {};

const handler = {
  registerSocket: registerSocket,
  removeSocket: removeSocket,
};

function registerSocket(skt) {
  sockets[skt.id] = skt;
}

function removeSocket(skt) {
  delete sockets[skt.id];
}

function publishEvent(type, data) {
  Object.values(sockets).map(socket => {
    socket.emit(type, data);
  });
}

function getIcon(id) {
  if (id.indexOf('truck') !== -1) {
    return '/images/truck.svg'
  } else if (id.indexOf('moto') !== -1) {
    return '/images/moto.svg'
  } else {
    return '/images/car.svg'
  }
}

function sendLog(data, type) {
  const payload = {
    sender: 'poll-dashboard',
    framework: 'nodejs',
    type: type,
    messageId: data.data.messageId,
    timestamp: new Date().toISOString(),
  };

  const serializedPayload = Buffer.from(JSON.stringify({data: payload}));
  nats.publish('logs', serializedPayload);
}

function parseEvent(data) {
  if (typeof data === 'string') {
    try {
      data = JSON.parse(data);
    } catch (e) {
      console.error('Parsing event failed: ', e);
    }
  }

  return data;
}

nats.subscribe('simulation.start', event => {
  try {
    const simulationName = `interval:${event.data.interval} vehicles:${event.data.vehicles}`;
    logger.initialize(simulationName);
  } catch (e) {
    console.error('Parsing simulation.start event failed: ', e);
  }

});

nats.subscribe('location.matched', event => {
  try {
    const data = {
      id: event.data.carId,
      lat: parseFloat(event.data.route[1].lat),
      lng: parseFloat(event.data.route[1].lon),
      icon: getIcon(event.data.carId),
    };

    publishEvent('location.update', data);
  } catch (e) {
    console.error('Parsing location.matched event failed: ', e);
  }

});

nats.subscribe('simulation.reset', () => {
  publishEvent('simulation.reset');
});

nats.subscribe('toll.calculated', (data) => {
  const event = parseEvent(data);
  publishEvent('toll.calculated', event);

  try {
    sendLog(event, 'received');
  } catch (e) {
    console.error('Saving logs for toll.calculated event failed: ', e);
  }
});

nats.subscribe('>', (data) => {
  const event = parseEvent(data);
  publishEvent('nats', event);
});

const publishStats = throttle(() => {
  const stats = logger.getStats();
  publishEvent('statistics', stats);
}, 1000);

nats.subscribe('logs', (data) => {
  try {
    logger.log(data.data);
  } catch (e) {
    console.error('Writing logs failed: ', e);
  }

  publishStats();
});

module.exports = handler;