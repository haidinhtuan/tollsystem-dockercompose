const fs = require('fs');
const readline = require('readline');
const path = require('path');
const FILE_PATH = process.env.LOG_FILE || 'logs';
const folderPath = path.join(__dirname, '..', FILE_PATH);

let stream;
let lastEvents;
let stats;

const logger = {
  initialize: initialize,
  log: log,
  getFileNames: getFileNames,
  getFile: getFile,
  getStats: getStats,
};

function initialize(simulationName) {
  if (stream) {
    cleanUp();
  }

  lastEvents = [];
  stats = {};

  const fileName = `${new Date().toLocaleString()} ${simulationName}`;
  const logFilePath = path.join(folderPath, fileName);
  stream = fs.createWriteStream(logFilePath, {flags: 'a'});

  // close stream before exit
  process.on('exit', cleanUp);
  process.on('SIGINT', cleanUp);
  process.on('SIGUSR1', cleanUp);
  process.on('SIGUSR2', cleanUp);
}

// Logs one event as one line in the current logfile, passes the event on to statistics generation
function log(data) {
  if (stream) {
    let line;
    if (data.type === 'step') {
      line = `${data.timestamp}, ${data.sender}, ${data.framework}, ${data.type}, ${data.step}, ${data.numberOfVehicles}\n`;
    } else {
      line = `${data.timestamp}, ${data.sender}, ${data.framework}, ${data.type}, ${data.messageId}\n`;
    }
    stream.write(line);
  }

  handleEvent(data, lastEvents, stats);
}

function cleanUp() {
  if (stream) {
    stream.end();
    stream = undefined;
  }

  if (stats) {
    stats = undefined;
  }

  if (lastEvents) {
    lastEvents = undefined;
  }
}

// return all available log file names
function getFileNames() {
  const fileNames = [];
  if (folderPath) {
    fs.readdirSync(folderPath).forEach(file => {
      fileNames.push(file);
    });
  }

  return fileNames;
}

// load log file and return generated statistics
async function getFile(fileName) {
  const logFilePath = path.join(folderPath, fileName);
  let lineReader;

  try {
    lineReader = readline.createInterface({input: fs.createReadStream(logFilePath)});
  } catch (err) {
    throw err;
  }

  const fileStats = {};
  const eventList = [];
  lineReader.on('line', (line) => {
    const data = line.split(', ');

    let event;
    if (data[3] === 'step') {
      event = {
        timestamp: data[0],
        sender: data[1],
        framework: data[2],
        type: data[3],
        step: parseInt(data[4]),
        numberOfVehicles: parseInt(data[5]),
      };
    } else {
      event = {
        timestamp: data[0],
        sender: data[1],
        framework: data[2],
        type: data[3],
        messageId: data[4],
      };
    }

    handleEvent(event, eventList, fileStats);
  });


  return new Promise((resolve) => {
    lineReader.on('close', () => {
      resolve(prepareStats(fileStats));
    });
  });
}

function saveStepInformation(event, stats) {
  stats.steps = stats.steps || [];
  stats.steps.push({
    step: event.step,
    numberOfVehicles: event.numberOfVehicles,
  });
}

// save event in current event list and pass on to data preparation
function handleEvent(event, eventList = [], statsObject = {}) {
  if (event.type === 'step') {
    saveStepInformation(event, statsObject);
    return;
  }

  if (!eventList[event.messageId]) {
    eventList[event.messageId] = [];
  }

  eventList[event.messageId].push(event);

  prepareData(parseInt(event.messageId), eventList[event.messageId], statsObject);
}

// parse current data for aggregation and trigger aggregation afterwards
function prepareData(messageId, data, statsObject) {
  const preparedData = {
    total: 0,
    services: {},
  };
  const services = {};

  // parse timestamps
  data.forEach(event => {
    services[event.sender] = services[event.sender] || {};

    if (event.type === 'received') {
      services[event.sender].received = new Date(event.timestamp).getTime();
    } else if (event.type === 'sent') {
      services[event.sender].sent = new Date(event.timestamp).getTime();
    } else {
      services[event.sender].sent = new Date(event.timestamp).getTime();
    }

    // calculate time spent in each service
    if (services[event.sender].sent && services[event.sender].received) {
      preparedData.services[event.sender] = services[event.sender].sent - services[event.sender].received;
    }
  });

  // calculate total delay
  if (services['poll-dashboard'] && services['toll-simulator']) {
    preparedData.services.total = services['poll-dashboard'].received - services['toll-simulator'].sent;
    aggregate(messageId, preparedData, statsObject);
  }
}

// aggregate current data to statistics
function aggregate(messageId, data, statsObject) {
  statsObject.services = statsObject.services || {};

  Object.keys(data.services).forEach(service => {
    statsObject.services[service] = statsObject.services[service] || [];
    if (!statsObject.services[service].find(item => item.x === messageId)) {
      statsObject.services[service].push({x: messageId, y: data.services[service]});
    }
  });
}

// return current live statistics
function getStats() {
  return prepareStats(stats);
}

function prepareStats(stats) {
  sortStats(stats);
  return calculateAverages(stats);
}

function sortStats(stats) {
  if (!stats.services) {
    return stats;
  }

  Object.keys(stats.services).forEach(service => {
    stats.services[service] = stats.services[service].sort((a, b) => a.x - b.x);
  });
}

function calculateAverages(stats) {
  stats.averages = stats.averages || {};
  let totalValues = {};
  if (!stats.services) {
    return stats;
  }

  Object.keys(stats.services).forEach(service => {
    totalValues[service] = totalValues[service] || {};
    totalValues[service].total = stats.services[service].reduce((accumulator, item) => accumulator + item.y, 0);
    totalValues[service].count = stats.services[service].length;
  });

  Object.keys(totalValues).forEach(service => {
    stats.averages[service] = Math.round((totalValues[service].total / totalValues[service].count) * 100) / 100;
  });

  return stats;
}

module.exports = logger;